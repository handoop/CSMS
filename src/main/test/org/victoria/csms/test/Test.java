package org.victoria.csms.test;

import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.util.DigestUtils;
import org.victoria.csms.domain.User;
import org.victoria.csms.service.UserService;
import org.victoria.csms.service.imp.UserServiceImp;

import java.util.Date;

/**
 * Created by thanatos on 15-6-27.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = "classpath:spring-context.xml")
public class Test {

    @Autowired private UserService __user__;

    @org.junit.Test
    public void test1(){
        User user = new User();
        user.setCreateTime(new Date());
        user.setName("victoria");
        user.setPassword(DigestUtils.md5DigestAsHex("123".getBytes()));
        user.setUsername("victoria");
        __user__.saveEntity(user);
    }

}
