<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<HTML>
<HEAD>
    <TITLE>服装库存管理系统</TITLE>
    <META HTTP-EQUIV="Content-Type" CONTENT="text/html; charset=gb2312">
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/frame.css">
    <script type="text/javascript" src="${pageContext.request.contextPath}/js/jquery-1.11.1.min.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/js/jquery.md5.js"></script>
</HEAD>
<body BACKGROUND="${pageContext.request.contextPath}/images/bg.gif">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
    <tr>
        <td width="100%" height="32">
            <TABLE WIDTH=100% BORDER=0 CELLPADDING=0 CELLSPACING=0>
                <TR>
                    <TD width="376"><IMG SRC="${pageContext.request.contextPath}/images/top_1.jpg" WIDTH=376 HEIGHT=54
                                         ALT=""></TD>
                    <TD width="90%" align="right" valign="bottom"
                        background="${pageContext.request.contextPath}/images/top_2.jpg">
                        <table width="100%" border="0" align="right" cellpadding="0" cellspacing="0">
                            <tr>
                                <td width="5%" align="center">&nbsp;</td>
                                <td width="80%" align="right">&nbsp;</td>
                                <td width="27%" align="right">
                                    <table width="75%" border="0" cellspacing="0" cellpadding="0">
                                        <tr>
                                            <td width="128" height="24" align="right"></td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </TD>
                </TR>
            </TABLE>
        </td>
    </tr>

</table>

<form name="login_form" method="POST" action="${pageContext.request.contextPath}/login.htm">
    <table width="100%" height="40%" border="0" cellpadding="0" cellspacing="0">
        <tr>
            <td align="center" valign="middle">
                <table width="300" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                        <td><label>用户名: </label>
                            <input name="username" type="text" size="20" required>

                        </td>
                        </tr>
                    <tr>
                        <td><label>&nbsp;密码:</label>
                            <input name="password" type="password" size="20" required>
                        </td>
                        </tr>
                    <tr>
                        <td width="42">
                            <input id="_submit" type="button" value="登录" alt="登录" width="42" height="32">
                            <input type="submit" style="display: none">
                        </td>
                    </tr>
                        <c:forEach items="${errors}" var="item">
                    <tr>
                            <label style="color: red;font-size: 11px;">${item.value}</label>
                    </tr>
                        </c:forEach>
                </table>
            </td>
        </tr>
    </table>
</form>
<script type="text/javascript">
    $('#_submit').click(function () {
        var form = document.forms.login_form
        var password = form.password.value
        form.password.value = $.md5(password)
        form.submit()
    })
</script>
</body>
</HTML>

