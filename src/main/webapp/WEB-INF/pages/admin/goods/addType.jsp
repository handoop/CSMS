<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib prefix="mvc" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="jstl" uri="http://java.sun.com/jsp/jstl/core" %>
<HTML>
<head>
    <META HTTP-EQUIV="content-type" CONTENT="text/html; charset=gb2312">
    <META HTTP-EQUIV="content-script-type" CONTENT="text/JavaScript">
    <META HTTP-EQUIV="content-style-type" CONTENT="text/css">
    <title>品牌修改</title>
    <link rel="stylesheet" href="${pageContext.request.contextPath}/css/cjpm.css">
    <script type="text/javascript" src="${pageContext.request.contextPath}/js/cjcalendar.js"></script>
    <script language="javascript" src="${pageContext.request.contextPath}/js/page.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/js/jquery-1.11.1.min.js"></script>
    <script src="${pageContext.request.contextPath}/js/jquery.validate.min.js"></script>
    <script src="${pageContext.request.contextPath}/js/jquery.metadata.js"></script>
    <script src="${pageContext.request.contextPath}/js/messages_cn.js"></script>
</head>
<SCRIPT type="text/javascript">
    function delCom(id) {
        if (id == '1') {
            document.idFrmMain.gys.value = "";
        } else {
            document.idFrmMain.sccj.value = "";
        }
    }

    function save() {

        alert("保存成功！");
    }

    function back() {
        history.back();
    }

</SCRIPT>
<script type="text/javascript">
    $(function(){
        var validate = $("#idFrmMain").validate({
            debug: true, //调试模式取消submit的默认提交功能
            //errorClass: "label.error", //默认为错误的样式类为：error
            focusInvalid: false, //当为false时，验证无效时，没有焦点响应
            onkeyup: false,
            submitHandler: function(form){   //表单提交句柄,为一回调函数，带一个参数：form
                alert("提交表单");
                form.submit();   //提交表单
            },

            rules:{
                goods_color:{
                    required:true
                },
                goods_size:{
                    required:true
                },
                factoryPrice:{
                    number:true
                },
                retailPrice:{
                    number:true
                }

            }

        });

    });
</script>
<BODY BACKGROUND="${pageContext.request.contextPath}/images/bg.gif">
<mvc:form name="idFrmMain" id="idFrmMain" method="POST" action="/admin/goods/addType.htm" modelAttribute="goods">
    <%--<FORM NAME="idFrmMain" ID="idFrmMain" METHOD="POST"  ACTION="" ONSUBMIT="return false"> --%>
    <table border="0" width="100%">
        <tr>
            <td width="100%" colspan="0" rowspan="0" align="center" valign="center">
                <table border="0" width="100%" id="table1" cellspacing="0" cellpadding="2" bgcolor="gray">
                    <tr>
                        <td class="headerbar61" width="50%">货号详细</td>
                        <td class="headerbar63" width="50%"><input type="submit" name="save70302002" value=" 保 存 ">
                            <input type="button" name="save70302002" onClick="javascript:back()" value=" 返 回 ">
                            &nbsp; </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td width="100%" colspan="0" rowspan="0" align="center" valign="center">
                <table border="0" width="100%" id="table1" cellspacing="1" cellpadding="2" bgcolor="gray">
                    <tr>
                        <td class="textbar81" width="15%">货号</td>
                        <td class="textbar01" width="35%">
                                <%--<input type="text" value="BR1703" size="15" style="width:210px " >--%>
                            <mvc:input path="goods_number" id="goods_number" size="15" value="${goods_number}" cssStyle="width: 210px" readonly="true"/>
                        </td>
                        <td class="textbar81" width="15%">品名</td>
                        <td class="textbar01" width="35%">
                                <%--<input type="text" value="中款大衣" size="15" style="width:210px "> --%>
                            <mvc:input path="goods_name" id="goods_name" size="15" value="${goods_name}" cssStyle="width: 210px" required="required"/>
                        </td>
                    </tr>
                    <tr>
                        <td width="15%" class="textbar81">色号</td>
                        <td class="textbar01" width="35%">
                            <mvc:select path="goods_color" id="goods_color" value="${goods_color}" cssStyle="width: 210px">
                            <mvc:option value="">请选择</mvc:option>
                            <jstl:forEach items="${colors}" var="item" varStatus="index">
                                <mvc:option value="${item.color}">${item.color}</mvc:option>
                            </jstl:forEach>
                            </mvc:select>
                                <%--&lt;%&ndash;<select name="" style="width:210px "> &ndash;%&gt;--%>
                                <%--&lt;%&ndash;<option value="" selected="selected">请选择</option> &ndash;%&gt;--%>
                                <%--&lt;%&ndash;<option value="1">大红色</option> &ndash;%&gt;--%>
                                <%--&lt;%&ndash;<option value="2">浅红色</option> &ndash;%&gt;--%>
                                <%--&lt;%&ndash;<option value="3">紫红色</option> &ndash;%&gt;--%>
                                <%--&lt;%&ndash;<option value="4">纯白色</option> &ndash;%&gt;--%>
                                <%--&lt;%&ndash;<option value="5">米白色</option> &ndash;%&gt;--%>
                                <%--&lt;%&ndash;<option value="6">深蓝色</option> &ndash;%&gt;--%>
                                <%--&lt;%&ndash;<option value="7">淡蓝色</option> &ndash;%&gt;--%>
                                <%--&lt;%&ndash;<option value="8">黑色</option> &ndash;%&gt;--%>
                                <%--&lt;%&ndash;<option value="9">棕色</option> &ndash;%&gt;--%>
                                <%--&lt;%&ndash;</select>&ndash;%&gt;--%>
                                <%--</td>--%>

                        <td width="15%" class="textbar81">尺码</td>
                        <td class="textbar01" width="35%">
                            <mvc:select path="goods_size" id="goods_size" value="${goods_size}" cssStyle="width: 210px">
                                <mvc:option value="">请选择</mvc:option>
                                <jstl:forEach items="${sizes}" var="item" varStatus="index">
                                    <mvc:option value="${item.size}">${item.size}</mvc:option>
                                </jstl:forEach>
                            </mvc:select>

                                <%--&lt;%&ndash;<select name="" style="width:210px "> &ndash;%&gt;--%>
                                <%--&lt;%&ndash;<option value="" selected="selected">请选择</option> &ndash;%&gt;--%>
                                <%--&lt;%&ndash;<option value="150">150</option> &ndash;%&gt;--%>
                                <%--&lt;%&ndash;<option value="155">155</option> &ndash;%&gt;--%>
                                <%--&lt;%&ndash;<option value="160">160</option> &ndash;%&gt;--%>
                                <%--&lt;%&ndash;<option value="165">165</option> &ndash;%&gt;--%>
                                <%--&lt;%&ndash;<option value="170">170</option> &ndash;%&gt;--%>
                                <%--&lt;%&ndash;<option value="175">175</option> &ndash;%&gt;--%>
                                <%--&lt;%&ndash;<option value="180">180</option> &ndash;%&gt;--%>
                                <%--&lt;%&ndash;<option value="185">185</option> &ndash;%&gt;--%>
                                <%--&lt;%&ndash;<option value="190">190</option> &ndash;%&gt;--%>
                                <%--&lt;%&ndash;</select> &ndash;%&gt;--%>
                        </td>
                    </tr>
                    <tr>
                        <td width="15%" class="textbar81">面料</td>
                        <td class="textbar01" width="35%">
                                <%--<input type="text" value="布" size="15" style="width:210px ">--%>
                            <mvc:input path="fabric" id="fabric" size="15" value="${fabric}" cssStyle="width: 210px" required="required"/>
                        </td>
                        <td class="textbar81" width="15%">里料</td>
                        <td class="textbar01" width="35%">
                                <%--<input type="text" value="尼龙" size="15" style="width:210px ">--%>
                            <mvc:input path="material" id="material" size="15" value="${material}" cssStyle="width: 210px" required="required"/>
                        </td>
                    </tr>
                    <tr>
                        <td width="15%" class="textbar81">出厂价</td>
                        <td class="textbar01" width="35%">
                                <%--<input type="text" value="450.00" size="15" style="width:210px ">--%>
                            <mvc:input path="factoryPrice" id="factoryPrice" size="15" value="${factoryPrice}" cssStyle="width: 210px"
                                       required="required"/>
                        </td>
                        <td class="textbar81" width="15%">零售价</td>
                        <td class="textbar01" width="35%">
                                <%--<input type="text" value="800.00" size="15" style="width:210px ">--%>
                            <mvc:input path="retailPrice" id="retailPrice" size="15" value="${retailPrice}" cssStyle="width: 210px" required="required"/>
                        </td>
                    </tr>


                </table>
                <table border=0 cellspacing=0 cellpadding=0 width="100%" height=5>
                    <tr>
                        <td></td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
    <%--</FORM> --%>
</mvc:form>
<script type="text/javascript">
    $(function(){
        var color = $("#goods_color").attr("value"),
                size = $("#goods_size").attr("value");
        $("#goods_color option").each(function(){
            if($(this).text() == color){
                $(this).attr("selected",true);
            }
        });

        $("#goods_size option").each(function(){
            if($(this).text() == size){
                $(this).attr("selected",true);
            }
        });
    })
</script>
</BODY>
</html>


