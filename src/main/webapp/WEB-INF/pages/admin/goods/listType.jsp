<%@ taglib prefix="jstl" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<HTML>
<head>
<META HTTP-EQUIV="content-type" CONTENT="text/html; charset=GB2312">
<META HTTP-EQUIV="content-script-type" CONTENT="text/JavaScript">
<META HTTP-EQUIV="content-style-type" CONTENT="text/css">
<title>日志查询</title>
<link rel="stylesheet" href="${pageContext.request.contextPath}/css/cjpm.css">
<script type="text/javascript" src="${pageContext.request.contextPath}/js/page.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/js/cjcalendar.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/js/jquery-1.11.1.min.js"></script>
</head>
<script language="javascript">
	var CalendarWebControl = new atCalendarControl();
</script>
<SCRIPT LANGUAGE="javaScript">
<!--
function goto(strURL)
{
	document.forms[0].action=strURL;
	document.forms[0].submit();
}
function del(id)
{
    if (confirm("您确定删除该条数据？")) {
        $.ajax({
            url: "/admin/goods/delete.htm",
            type: "post",
            data: {id: id},
            success: function (data) {
                if (data.code == 0) {
                    alert("delete successful!")
                    window.location.reload()
                }
                if (data.code == 1) {
                    alert(data.message)
                }
            }
        });
    }
}

function goSearch() {
    document.forms[0].action = "/admin/goods/listType.htm";
    document.forms[0].submit();
}

function doAdd(){
	document.forms[0].action="/admin/goods/addType.htm";
    document.forms[0].method = "GET";
	document.forms[0].submit();
}
 
-->
</SCRIPT>

<BODY BACKGROUND="${pageContext.request.contextPath}/images/bg.gif">
<FORM NAME="idFrmMain" ID="idmig0101" METHOD="POST"  ACTION="" ONSUBMIT="return false" >

<table border=0 cellspacing=0 cellpadding=2 width="100%" bgcolor="gray">
<tr>
	<td class="headerbar61">货号查询</td>
    <td class="headerbar63" width="50%" colspan="1"><p align="right">
    	<input type=submit value=" 查 询 " onClick="goSearch();"></p></td>
  </tr>
</tr>
</table>

<table border=0 cellspacing=0 cellpadding=2 width="100%" height="5">
<tr>
	<td></td>
</tr>
</table>

<table border=0 cellspacing=1 cellpadding=2 width="100%" bgcolor="gray">
<tr>

    <td class="textbar81" width="15%">货号</td>
		<td class="textbar01" width="35%">			
		<input type="text" name="goods_number" value="${goods_number}" style="width:210px ">	  </td>
	<td class="textbar81" width="15%">品名</td>
		<td class="textbar01" width="35%">			
		<input type="text" name="goods_name" value="${goods_name}" style="width:210px ">	  </td>
</tr>   
   <tr> 
            <td width="15%" class="textbar81">色号</td> 
            <td class="textbar01" width="35%">
                <select name="goods_color" style="width:210px ">
                   <option value="" selected="selected">请选择</option>
                    <jstl:forEach items="${colors}" var="item" varStatus="index">
                        <option value="${item.color}">${item.color}</option>
                    </jstl:forEach>
                    <%--<option value="1">大红色</option>--%>
                    <%--<option value="2">浅红色</option>--%>
                    <%--<option value="3">紫红色</option>--%>
                    <%--<option value="4">纯白色</option>--%>
                    <%--<option value="5">米白色</option>--%>
                    <%--<option value="6">深蓝色</option>--%>
                    <%--<option value="7">淡蓝色</option>--%>
                    <%--<option value="8">黑色</option>--%>
                    <%--<option value="9">棕色</option>--%>
              </select></td> 
         
            <td width="15%" class="textbar81">尺码</td> 
            <td class="textbar01" width="35%"> <select name="goods_size" style="width:210px ">
               <option value="" selected="selected">请选择</option>
                <jstl:forEach items="${sizes}" var="item" varStatus="index">
                    <option value="${item.size}">${item.size}</option>
                </jstl:forEach>
                <%--<option value="150">150</option> --%>
                <%--<option value="155">155</option> --%>
                <%--<option value="160">160</option> --%>
				<%--<option value="165">165</option> --%>
                <%--<option value="170">170</option> --%>
				<%--<option value="175">175</option> --%>
                <%--<option value="180">180</option> --%>
				<%--<option value="185">185</option> --%>
                <%--<option value="190">190</option> --%>
              </select> </td>            
          </tr>  
</table>

<table border=0 cellspacing=0 cellpadding=0 width="100%" height=5>
<tr>
	<td></td>
</tr>
</table>

<table border="0" width="100%" id="table1" cellspacing="0"  cellpadding="2"  bgcolor="gray">
	<tr>
  	<td class="headerbar61" width="50%" colspan="1">货号明细</td>
    <td class="headerbar63" width="50%" colspan="1"><p align="right">
    	<input type=submit value=" 新 增 " onClick="JavaScript:doAdd();"></p></td>
  </tr>
</table>
  <table border=0 cellspacing=0 cellpadding=2 width="100%" height="5"> 
    <tr> 
      <td></td> 
    </tr> 
  </table> 
<table border="0" width="100%" id="table1" cellspacing="0"  cellpadding="0"  bgcolor="gray">
	<tr>
  	<td  width="100%" colspan="1">
  		<table  border="0" cellspacing="1" cellpadding="2" width="100%">
				<tr>
					<td  width="5%" class="headerbar82">序号</td>
					<td  width="10%" class="headerbar82">货号</td>
					<td  width="10%" class="headerbar82">色号</td>
					<td  width="10%" class="headerbar82">尺码</td>
					<td  width="15%" class="headerbar82">品名</td>
					<td  width="10%" class="headerbar82">面料</td>
					<td  width="10%" class="headerbar82">里料</td>
                    <td  width="10%" class="headerbar82">出厂价</td>
					<td  width="10%" class="headerbar82">零售价</td>
					<td   class="headerbar82">操作</td>
				</tr>

            <jstl:forEach items="${page.recordList}" var="item" varStatus="index">
                <tr>
                    <td class="gridbar11" align="center"> ${index.index+1}</td>
                    <td  class="gridbar11" align="center"> <a href="/admin/goods/detail.htm?id=${item.goodsID}">${item.goods_number}</a></td>
                    <td  class="gridbar11" align="center"> ${item.goods_color}</td>
                    <td  class="gridbar11" align="center"> ${item.goods_size}</td>
                    <td  class="gridbar11" align="center"> ${item.goods_name}</td>
                    <td  class="gridbar11" align="center"> ${item.fabric}</td>
                    <td  class="gridbar11" align="center"> ${item.material}</td>
                    <td   class="gridbar11" align="center"> ${item.factoryPrice}</td>
                    <td   class="gridbar11" align="center"> ${item.retailPrice}</td>
                    <td  class="gridbar11" align="center">
                        <img src="${pageContext.request.contextPath}/images/del.gif" align="bottom" border="0" alt="删除" onClick="del(${item.goodsID})"  style="cursor:hand"/></td>
                </tr>
            </jstl:forEach>

				<%--<tr>--%>
					<%--<td class="gridbar11" align="center"> 1</td>--%>
					<%--<td  class="gridbar11" align="center"> <a href="list.htm">BR1703</a></td>--%>
					<%--<td  class="gridbar11" align="center"> 大红色</td>--%>
					<%--<td  class="gridbar11" align="center"> 160</td>--%>
					<%--<td  class="gridbar11" align="center"> 中款大衣</td>--%>
					<%--<td  class="gridbar11" align="center"> 布</td>--%>
					<%--<td  class="gridbar11" align="center"> 尼龙</td>--%>
                    <%--<td   class="gridbar11" align="center"> 450.00</td>--%>
					<%--<td   class="gridbar11" align="center"> 800.00</td>--%>
					<%--<td  class="gridbar11" align="center">--%>
						<%--<img src="${pageContext.request.contextPath}/images/del.gif" align="bottom" border="0" alt="删除" onClick="javascript:del('673467')"  style="cursor:hand"/></td>--%>
				<%--</tr>--%>
				<%--<tr>--%>
					<%--<td class="gridbar01" align="center"> 2</td>--%>
					<%--<td  class="gridbar01" align="center"> <a href="#"  onclick="javascript:goto('add.jsp');">BR1704</a></td>--%>
					<%--<td  class="gridbar01" align="center"> 大红色</td>--%>
					<%--<td  class="gridbar01" align="center"> 160</td>--%>
					<%--<td  class="gridbar01" align="center"> 中款大衣</td>--%>
					<%--<td  class="gridbar01" align="center"> 布</td>--%>
					<%--<td  class="gridbar01" align="center"> 尼龙</td>--%>
                    <%--<td   class="gridbar01" align="center"> 450.00</td>--%>
					<%--<td   class="gridbar01" align="center"> 800.00</td>--%>
					<%--<td  class="gridbar01" align="center">--%>
						<%--<img src="${pageContext.request.contextPath}/images/del.gif" align="bottom" border="0" alt="删除" onClick="javascript:del('673467')"  style="cursor:hand"/></td>--%>
				<%--</tr>--%>
				<%--<tr>--%>
					<%--<td class="gridbar11" align="center"> 3</td>--%>
					<%--<td  class="gridbar11" align="center"> <a href="#"  onclick="javascript:goto('add.jsp');">BR1705</a></td>--%>
					<%--<td  class="gridbar11" align="center"> 大红色</td>--%>
					<%--<td  class="gridbar11" align="center"> 160</td>--%>
					<%--<td  class="gridbar11" align="center"> 中款大衣</td>--%>
					<%--<td  class="gridbar11" align="center"> 布</td>--%>
					<%--<td  class="gridbar11" align="center"> 尼龙</td>--%>
                    <%--<td   class="gridbar11" align="center"> 450.00</td>--%>
					<%--<td   class="gridbar11" align="center"> 800.00</td>--%>
					<%--<td  class="gridbar11" align="center">--%>
						<%--<img src="${pageContext.request.contextPath}/images/del.gif" align="bottom" border="0" alt="删除" onClick="javascript:del('673467')"  style="cursor:hand"/></td>--%>
				<%--</tr>--%>
				<%--<tr>--%>
					<%--<td class="gridbar01" align="center"> 4</td>--%>
					<%--<td  class="gridbar01" align="center"> <a href="#"  onclick="javascript:goto('add.jsp');">BR1706</a></td>--%>
					<%--<td  class="gridbar01" align="center"> 大红色</td>--%>
					<%--<td  class="gridbar01" align="center"> 160</td>--%>
					<%--<td  class="gridbar01" align="center"> 中款大衣</td>--%>
					<%--<td  class="gridbar01" align="center"> 布</td>--%>
					<%--<td  class="gridbar01" align="center"> 尼龙</td>--%>
                    <%--<td   class="gridbar01" align="center"> 450.00</td>--%>
					<%--<td   class="gridbar01" align="center"> 800.00</td>--%>
					<%--<td  class="gridbar01" align="center">--%>
						<%--<img src="${pageContext.request.contextPath}/images/del.gif" align="bottom" border="0" alt="删除" onClick="javascript:del('673467')"  style="cursor:hand"/></td>--%>
				<%--</tr>--%>
		</table>
		
	  </td>
	</tr>
</table>
<table width="100%" border="0" cellpadding="1" cellspacing="2" >
        	<tr>
                <td colspan="2" align="right" height="20" nowrap class="textbar3">
                    &nbsp; 共${page.recordCount}条 &nbsp; 第${page.currentPage}/${page.pageCount}页 &nbsp;
                    <jstl:if test="${page.currentPage>2}">
                        <a style="cursor:hand"
                           href="${pageContext.request.contextPath}/admin/goods/listType.htm?pageNum=1&goods_number=${goods_number}&goods_name=${goods_name}&goods_color=${goods_color}&goods_size=${goods_size}">首页</a>&nbsp;
                    </jstl:if>
                    <jstl:if test="${page.currentPage>1}">
                        <a style="cursor:hand"
                           href="${pageContext.request.contextPath}/admin/goods/listType.htm?pageNum=${page.currentPage-1}&goods_number=${goods_number}&goods_name=${goods_name}&goods_color=${goods_color}&goods_size=${goods_size}">上一页</a>&nbsp;
                    </jstl:if>
                    <jstl:forEach begin="${page.beginIndex}" end="${page.endIndex}" varStatus="step">
                        <a style="cursor:hand"
                           href="${pageContext.request.contextPath}/admin/goods/listType.htm?pageNum=${step.index}&goods_number=${goods_number}&goods_name=${goods_name}&goods_color=${goods_color}&goods_size=${goods_size}">${step.index}</a>&nbsp;
                    </jstl:forEach>
                    <jstl:if test="${page.currentPage<page.pageCount}">
                        <a style="cursor:hand"
                           href="${pageContext.request.contextPath}/admin/goods/listType.htm?pageNum=${page.currentPage+1}&goods_number=${goods_number}&goods_name=${goods_name}&goods_color=${goods_color}&goods_size=${goods_size}">下一页</a>&nbsp;
                    </jstl:if>
                    <jstl:if test="${page.currentPage+1<page.pageCount}">
                        <a style="cursor:hand"
                           href="${pageContext.request.contextPath}/admin/goods/listType.htm?pageNum=${page.pageCount}&goods_number=${goods_number}&goods_name=${goods_name}&goods_color=${goods_color}&goods_size=${goods_size}">尾页</a>&nbsp;
                    </jstl:if>
                </td>
          </tr>
        </table>

			</td>
		</tr>
</table>
</FORM>
</BODY>
</html>

