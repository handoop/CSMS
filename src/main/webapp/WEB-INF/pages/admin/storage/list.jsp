<%@ taglib prefix="jstl" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<HTML>
<head>
<META HTTP-EQUIV="content-type" CONTENT="text/html; charset=GB2312">
<META HTTP-EQUIV="content-script-type" CONTENT="text/JavaScript">
<META HTTP-EQUIV="content-style-type" CONTENT="text/css">
<title>日志查询</title>
<link rel="stylesheet" href="${pageContext.request.contextPath}/css/cjpm.css">
<script type="text/javascript" src="${pageContext.request.contextPath}/js/page.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/js/cjcalendar.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/js/jquery-1.11.1.min.js"></script>
</head>
<script language="javascript">
	var CalendarWebControl = new atCalendarControl();
</script>
<SCRIPT LANGUAGE="javaScript">
<!--
//function goto(strURL)
//{
//	document.forms[0].action=strURL;
//	document.forms[0].submit();
//}

function goto() {
    document.forms[0].action = "/admin/storage/add.htm";
    document.forms[0].method = "GET";
    document.forms[0].submit();
}

function del(id) {
    if (confirm("您确定删除该条数据？")) {
        $.ajax({
            url: "/admin/storage/delete.htm",
            type: "post",
            data: {id: id},
            success: function (data) {
                if (data.code == 0) {
                    alert("delete successful!")
                    window.location.reload()
                }
                if (data.code == 1) {
                    alert(data.message)
                }
            }
        });
    }
}

function goSearch() {
    document.forms[0].action = "/admin/storage/list.htm";
    document.forms[0].submit();
}

 
 
-->
</SCRIPT>

<BODY BACKGROUND="${pageContext.request.contextPath}/images/bg.gif">
<FORM NAME="idFrmMain" ID="idmig0101" METHOD="POST"  ACTION="" ONSUBMIT="return false" >


<table border=0 cellspacing=0 cellpadding=2 width="100%" bgcolor="gray">
<tr>
	<td class="headerbar61">仓库查询</td>
    <td class="headerbar63" width="50%" colspan="1"><p align="right">
    	<input type=submit value=" 查 询 " onClick="goSearch();"></p></td>
  </tr>
</tr>
</table>

<table border=0 cellspacing=0 cellpadding=2 width="100%" height="5">
<tr>
	<td></td>
</tr>
</table>

<table border=0 cellspacing=1 cellpadding=2 width="100%" bgcolor="gray">
<tr>

    <td class="textbar81" width="15%">仓库编号</td>
		<td class="textbar01" width="35%">			
		<input type="text" name="sto_number" value="${sto_number}" style="width:210px ">	  </td>
	<td class="textbar81" width="15%">仓库名称</td>
		<td class="textbar01" width="35%">			
		<input type="text" name="sto_name" value="${sto_name}" style="width:210px ">	  </td>
</tr>   
   
</table>

<table border=0 cellspacing=0 cellpadding=0 width="100%" height=5>
<tr>
	<td></td>
</tr>
</table>

<table border="0" width="100%" id="table1" cellspacing="0"  cellpadding="2"  bgcolor="gray">
	<tr>
  	<td class="headerbar61" width="50%" colspan="1">仓库明细</td>
    <td class="headerbar63" width="50%" colspan="1"><p align="right">
    	<input type=submit value=" 新 增 " onclick="JavaScript:goto();"></p></td>
  </tr>
</table>
  <table border=0 cellspacing=0 cellpadding=2 width="100%" height="5"> 
    <tr> 
      <td></td> 
    </tr> 
  </table> 
<table border="0" width="100%" id="table1" cellspacing="0"  cellpadding="0"  bgcolor="gray">
	<tr>
  	<td  width="100%" colspan="1">
  		<table  border="0" cellspacing="1" cellpadding="2" width="100%">
				<tr>
					<td  width="5%" class="headerbar82">序号</td>
					<td  width="10%" class="headerbar82">仓库编号</td>
					<td  width="20%" class="headerbar82">仓库名称</td>
					<td  width="20%" class="headerbar82">联系人</td>
					<td  width="20%" class="headerbar82">联系电话</td>
					<%--<td  width="10%" class="headerbar82">仓储量</td>--%>
					<td  width="5%" class="headerbar82">操作</td>
				</tr>

            <jstl:forEach items="${page.recordList}" var="item" varStatus="index">
                <tr>
                    <td width="5%" class="gridbar11">${index.index+1}</td>
                    <td width="10%" class="gridbar11"><a href="/admin/storage/detail.htm?id=${item.id}">${item.sto_number}</a></td>
                    <td width="20%" class="gridbar11">${item.sto_name}</td>
                    <td width="20%" class="gridbar11">${item.connect_man}</td>
                    <td width="20%" class="gridbar11">${item.connect_phone}</td>
                    <%--<td width="10%" class="gridbar11">${item.sto_capacity}</td>--%>
                    <td width="5%" class="gridbar11">
                        <a href="#"><img src="${pageContext.request.contextPath}/images/del.gif" align="bottom"
                                         border="0" alt="删除" onClick="del(${item.id})" style="cursor:hand"/></a></td>
                </tr>
            </jstl:forEach>

				<%--<tr>--%>
					<%--<td  width="5%" class="gridbar11"> 1</td>--%>
					<%--<td  width="10%" class="gridbar11"> <a href="#"  onclick="javascript:goto('add.jsp');">10001</a></td>--%>
					<%--<td  width="20%" class="gridbar11"> 高邮仓库</td>--%>
					<%--<td  width="10%" class="gridbar11"> 600</td>--%>
					<%--<td  width="5%" class="gridbar11">--%>
						 <%--<img src="${pageContext.request.contextPath}/images/del.gif" align="bottom" border="0" alt="删除" onclick="javascript:del('673467')"  style="cursor:hand"/></td>--%>

					<%--</td>--%>
				<%--</tr>--%>
			</table>
		</td>
	</tr>
</table>

<table width="100%" border="0" cellpadding="1" cellspacing="2" >
        	<tr>
                <td colspan="2" align="right" height="20" nowrap class="textbar3">
                    &nbsp; 共${page.recordCount}条 &nbsp; 第${page.currentPage}/${page.pageCount}页 &nbsp;
                    <jstl:if test="${page.currentPage>2}">
                        <a style="cursor:hand"
                           href="${pageContext.request.contextPath}/admin/storage/list.htm?pageNum=1&sto_number=${sto_number}&sto_name=${sto_name}">首页</a>&nbsp;
                    </jstl:if>
                    <jstl:if test="${page.currentPage>1}">
                        <a style="cursor:hand"
                           href="${pageContext.request.contextPath}/admin/storage/list.htm?pageNum=${page.currentPage-1}&sto_number=${sto_number}&sto_name=${sto_name}">上一页</a>&nbsp;
                    </jstl:if>
                    <jstl:forEach begin="${page.beginIndex}" end="${page.endIndex}" varStatus="step">
                        <a style="cursor:hand"
                           href="${pageContext.request.contextPath}/admin/storage/list.htm?pageNum=${step.index}&sto_number=${sto_number}&sto_name=${sto_name}">${step.index}</a>&nbsp;
                    </jstl:forEach>
                    <jstl:if test="${page.currentPage<page.pageCount}">
                        <a style="cursor:hand"
                           href="${pageContext.request.contextPath}/admin/storage/list.htm?pageNum=${page.currentPage+1}&sto_number=${sto_number}&sto_name=${sto_name}">下一页</a>&nbsp;
                    </jstl:if>
                    <jstl:if test="${page.currentPage+1<page.pageCount}">
                        <a style="cursor:hand"
                           href="${pageContext.request.contextPath}/admin/storage/list.htm?pageNum=${page.pageCount}&name=${name}&username=${username}">尾页</a>&nbsp;
                    </jstl:if>
                </td>
            </tr>
        </table>

			</td>
		</tr>
</table>
</FORM>
</BODY>
</html>
