<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<HTML>
<head>
    <META HTTP-EQUIV="content-type" CONTENT="text/html; charset=UTF-8">
    <META HTTP-EQUIV="content-script-type" CONTENT="text/JavaScript">
    <META HTTP-EQUIV="content-style-type" CONTENT="text/css">
    <title>入库单</title>
    <link rel="stylesheet" href="${pageContext.request.contextPath}/css/cjpm.css">
    <script type="text/javascript" src="${pageContext.request.contextPath}/js/cjcalendar.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/js/addFunction.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/js/jquery-1.11.1.min.js"></script>
</head>
<script language="javascript">
    var CalendarWebControl = new atCalendarControl();
</script>
<SCRIPT LANGUAGE="javaScript">

    function goto(strURL) {
        document.forms[0].action = strURL;
        document.forms[0].submit();
    }

    function del(storageInDetailID, storageID) {
        if (confirm("您确定删除该条数据？")) {
            $.ajax({
                url: "/admin/stock/storageInDetailDel.htm",
                type: "post",
                data: {
                    storageInDetailID: storageInDetailID,
                    storageID: storageID,
                },
                success: function (data) {
                    if (data.code == 0) {
                        alert("delete successful!")
                        window.location.reload()
                    }
                    if (data.code == 1) {
                        alert(data.message)
                    }
                }
            });
        }
    }

    function goSearch() {
        document.forms[0].action = "order3002.htm";
        document.forms[0].submit();
    }


</SCRIPT>

<BODY BACKGROUND="${pageContext.request.contextPath}/images/bg.gif">
<FORM NAME="mig0101" ID="idmig0101" METHOD="POST" ACTION="" ONSUBMIT="return false">

    <table border=0 cellspacing=0 cellpadding=2 width="100%" bgcolor="gray">
        <tr>
            <td class="headerbar61">入单详细</td>

            <td class="headerbar61"><p align="right">
                <input type=button value=" 返 回 " onClick="JavaScript:history.back();">
            </p></td>
        </tr>
    </table>
    <table border=0 cellspacing=1 cellpadding=2 width="100%" bgcolor="gray">
        <tr>
            <td class="textbar81" width="15%">单据号</td>
            <td class="textbar01" width="35%">
                ${storageInDetail.storageIn_number}
            </td>
            <td class="textbar81" width="15%">单据日期</td>
            <td class="textbar01" width="35%">
                ${storageInDetail.storageIn_dateTime}
            </td>
        </tr>
        <tr>
            <td class="textbar81" width="15%">所入仓库</td>
            <td class="textbar01" width="35%">
                ${storageInDetail.storage.sto_name}</td>
            <td class="textbar81" width="15%">接收人</td>
            <td class="textbar01" width="35%">
                ${storageInDetail.receiver_man}</td>
        </tr>
        <tr>
            <td class="textbar81" width="15%">来源</td>
            <td class="textbar01" width="35%">
                ${storageInDetail.source_company}</td>

        </tr>
        <tr>
            <td class="textbar81" width="15%">备注</td>
            <td class="textbar01" colspan="3" width="85%">
                ${storageInDetail.storageIn_note}</td>
        </tr>
    </table>

    <table border=0 cellspacing=0 cellpadding=0 width="100%" height=5>
        <tr>
            <td></td>
        </tr>
    </table>

    <table border="0" width="100%" id="table" cellspacing="0" cellpadding="2" bgcolor="gray">
        <tr>
            <td class="headerbar61" width="100%" colspan="1">入库单明细</td>
            <td class="headerbar61">
                <p align="rigth">
                    <input type=submit value=" 新 增 "
                           onClick="JavaScript:goto('/admin/stock/showStorageInDetailAdd.htm?storageInID=${storageInDetail.storageInID}&storageID=${storageInDetail.storage.id}')">
                </p>
            </td>

    </table>

    <table id="tab0" border="0" cellspacing="1" cellpadding="2" width="100%" bgcolor="gray">
        <tr>
            <td width="5%" class="headerbar82">序号</td>
            <td width="20%" class="headerbar82">货号</td>
            <td width="20%" class="headerbar82">品名</td>
            <td width="15%" class="headerbar82">色号</td>
            <td width="15%" class="headerbar82">尺码</td>
            <td width="15%" class="headerbar82">数量</td>
            <td class="headerbar82">操作</td>
        </tr>
        <c:forEach items="${page.recordList}" var="item" varStatus="index">
            <tr>
                <td class="gridbar11" align="center">${index.count}</td>
                <td class="gridbar11" align="center">${item.goods.goods_number}</td>
                <td class="gridbar11">${item.goods.goods_name}</td>
                <td class="gridbar11">${item.goods.goods_color}</td>
                <td class="gridbar11">${item.goods.goods_size}</td>
                <td class="gridbar11" align="center">${item.storageInNum}</td>
                <td class="gridbar11" align="center">
                    <a href="#">
                        <img src="${pageContext.request.contextPath}/images/del.gif" align="bottom" border="0" alt="删除"
                             onClick="javascript:del(${item.storageInDetailID},${storageInDetail.storage.id})"/>
                    </a></td>
            </tr>
        </c:forEach>
    </table>

    <table width="100%" border="0" cellpadding="0" cellspacing="2">
        <tr>
            <td colspan="2" align="right" height="20" nowrap class="textbar3">
                &nbsp; ${storageInPage.pageCount} &nbsp;&nbsp; 第${page.currentPage}/${page.pageCount}页
                &nbsp;&nbsp;
                <c:if test="${page.currentPage>2}">
                    <a style="cursor:hand"
                       href="${pageContext.request.contextPath}/admin/stock/showStorageInDetail.htm?pageNum=1&id=${storageInDetail.storageInID}">首页</a>&nbsp;
                </c:if>
                <c:if test="${page.currentPage>1}">
                    <a style="cursor:hand"
                       href="${pageContext.request.contextPath}/admin/stock/showStorageInDetail.htm?pageNum=${page.currentPage-1}&id=${storageInDetail.storageInID}">上一页</a>&nbsp;
                </c:if>
                <c:forEach begin="${page.beginIndex}" end="${page.endIndex}" varStatus="step">
                    <a style="cursor:hand"
                       href="${pageContext.request.contextPath}/admin/stock/showStorageInDetail.htm?pageNum=${step.index}&id=${storageInDetail.storageInID}">${step.index}</a>&nbsp;
                </c:forEach>
                <c:if test="${page.currentPage<page.pageCount}">
                    <a style="cursor:hand"
                       href="${pageContext.request.contextPath}/admin/stock/showStorageInDetail.htm?pageNum=${page.currentPage+1}&id=${storageInDetail.storageInID}">下一页</a>&nbsp;
                </c:if>
                <c:if test="${page.currentPage+1<page.pageCount}">
                    <a style="cursor:hand"
                       href="${pageContext.request.contextPath}/admin/stock/showStorageInDetail.htm?pageNum=${page.pageCount}&id=${storageInDetail.storageInID}">尾页</a>&nbsp;s
                </c:if>
            </td>
        </tr>
    </table>

</FORM>
</BODY>
</html>

