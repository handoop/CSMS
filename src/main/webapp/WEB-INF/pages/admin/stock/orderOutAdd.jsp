<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<HTML>
<head>
<META HTTP-EQUIV="content-type" CONTENT="text/html; charset=GB2312">
<META HTTP-EQUIV="content-script-type" CONTENT="text/JavaScript">
<META HTTP-EQUIV="content-style-type" CONTENT="text/css">
	<script type="text/javascript" src="${pageContext.request.contextPath}/js/laydate.js"></script>
<title>出库单</title>
<link rel="stylesheet" href="${pageContext.request.contextPath}/css/cjpm.css">
<script type="text/javascript" src="${pageContext.request.contextPath}/js/cjcalendar.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/js/addFunction.js"></script>
	<script type="text/javascript" src="${pageContext.request.contextPath}/js/jquery-1.11.1.min.js"></script>
	<script src="${pageContext.request.contextPath}/js/jquery.validate.min.js"></script>
	<script src="${pageContext.request.contextPath}/js/jquery.metadata.js"></script>
	<script src="${pageContext.request.contextPath}/js/messages_cn.js"></script>
</head>
<script language="javascript">
	var CalendarWebControl = new atCalendarControl();
</script>
<SCRIPT LANGUAGE="javaScript">

function goto(strURL)
{
	document.forms[0].action=strURL;
	document.forms[0].submit();
}

function del(id)
{
	if(confirm("您确定删除该条明细？")){
		alert("删除成功！");
	}
}

function goSearch(){
	document.forms[0].action="order3002.htm";
	document.forms[0].submit();
}

$(function(){
	var validate = $("#orderOutAdd").validate({
		debug: true, //调试模式取消submit的默认提交功能
		//errorClass: "label.error", //默认为错误的样式类为：error
		focusInvalid: false, //当为false时，验证无效时，没有焦点响应
		onkeyup: false,
		submitHandler: function(form){   //表单提交句柄,为一回调函数，带一个参数：form
			alert("提交表单");
			form.submit();   //提交表单
		},

		rules:{
			storageOutDateTimeStr:{
				required:true
			},
			receiverName:{
				required:true
			},
			receiverPhone:{
				number:true,
				required:true

			},
			receiverAddress:{
				required:true
			},
			storeRoom:{
				required:true
			}

		}

	});

});
</SCRIPT>

<BODY BACKGROUND="${pageContext.request.contextPath}/images/bg.gif">
<FORM NAME="idFrmMain" ID="orderOutAdd" METHOD="POST"  ACTION="/admin/stock/addStorageOut.htm" ONSUBMIT="return false" >

<table border=0 cellspacing=0 cellpadding=2 width="100%" bgcolor="gray">
<tr>
	<td class="headerbar61">出库单详细</td>

	<td class="headerbar61"><p align="right">
		<input type="submit" value=" 保 存 " onClick="">
		<input type=button value=" 返 回 " onClick="JavaScript:history.back();">
    </p></td>
</tr>
</table>
<table border=0 cellspacing=0 cellpadding=2 width="100%" height="5">
<tr>
	<td></td>
</tr>
</table>
<table border=0 cellspacing=1 cellpadding=2 width="100%" bgcolor="gray">
<tr>
  <td class="textbar81" width="15%">单据号</td>
		<td class="textbar01" width="35%">
      <input type="text" name="storageOutNumber"  value="${storageOutNumber}" readonly style="width:200px">    </td>
    <td class="textbar81" width="15%">单据日期</td>
		<td class="textbar01" width="35%">
			<input id="storageOutDateTimeStr" name="storageOutDateTimeStr" placeholder="请输入日期" class="laydate-icon" onclick="laydate()" />

			<%--<input type="text" name="storageOutDateTimeStr" id="frmWRPT_OPT_DATE2_PJT70302" value="2007-06-21" readonly="readonly" size="12">--%>
			<%--<img src="${pageContext.request.contextPath}/images/calendar.gif" width="18" height="17" onClick="CalendarWebControl.show(forms[0].frmWRPT_OPT_DATE2_PJT70302,'',forms[0].frmWRPT_OPT_DATE2_PJT70302);" title="显示日历" />    </td>--%>
</tr>
<tr>
    <td class="textbar81" width="15%">所出仓库</td>
	<td class="textbar01" width="35%">
			<select name="storeRoom" id="storeRoom" style="width:200px">
				<option value="" selected>------</option>
				<c:forEach var="storage" items="${storageList}">
					<option value="${storage.id}">${storage.sto_name}</option>
				</c:forEach>
			</select>    </td>
    <td class="textbar81" width="15%">接收人</td>
	  <td class="textbar01" width="35%">
		  <input type="text" id="receiverName" name="receiverName" style="width:200px"></td>
</tr>
<tr>
    <td class="textbar81" width="15%">接收人电话</td>
				<td class="textbar01" width="35%">
      <input type="text" id="receiverPhone" name="receiverPhone" value="" style="width:200px">    </td>

    <td class="textbar81" width="15%">发往地址</td>
		<td class="textbar01"  width="35%">
      <input type="text" id="receiverAddress" name="receiverAddress" value="" style="width:200px">    </td>

</tr>
<tr>
		<td class="textbar81" width="15%">备注</td>
    <td class="textbar01" colspan="3" width="85%">
    	<textarea id="storageOutNote" name="storageOutNote" cols="80" rows="4"></textarea>    </td>
</tr>
</table>

<table border=0 cellspacing=0 cellpadding=0 width="100%" height=5>
<tr>
	<td></td>
</tr>
</table>


</FORM>
</BODY>
</html>

