<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<HTML>
<head>
    <META HTTP-EQUIV="content-type" CONTENT="text/html; charset=gb2312">
    <META HTTP-EQUIV="content-script-type" CONTENT="text/JavaScript">
    <META HTTP-EQUIV="content-style-type" CONTENT="text/css">
    <title>出库单详细</title>
    <link rel="stylesheet" href="${pageContext.request.contextPath}/css/cjpm.css">
    <script type="text/javascript" src="${pageContext.request.contextPath}/js/addFunction.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/js/jquery-1.11.1.min.js"></script>
    <script src="${pageContext.request.contextPath}/js/jquery.validate.min.js"></script>
    <script src="${pageContext.request.contextPath}/js/jquery.metadata.js"></script>
    <script src="${pageContext.request.contextPath}/js/messages_cn.js"></script>

</head>

<SCRIPT LANGUAGE="javaScript">
    <!--
    function save() {
        alert("保存成功！");
    }

    function back() {
        history.back();
    }

    -->
    function goto(strURL)
    {
        document.forms[0].action=strURL;
        document.forms[0].submit();
    }
    function selectChang(goods) {
        var str = goods.split("*");
        var color = document.getElementById("goodsColor");
        var size = document.getElementById("goodsSize");
        var outCount = document.getElementById("storageOutCount");
        var count = document.getElementById("storageCount");
//        alert("you select : "+str[0]+ " -- "+str[1]+" -- "+str[2]+" -- "+str[3]);
        color.value = str[1];
        size.value = str[2];
        count.value = str[3];
        outCount.value = "";

    }

    $(function(){
        var validate = $("#orderOutDetailAdd").validate({
            debug: true, //调试模式取消submit的默认提交功能
            //errorClass: "label.error", //默认为错误的样式类为：error
            focusInvalid: false, //当为false时，验证无效时，没有焦点响应
            onkeyup: false,
            submitHandler: function(form){   //表单提交句柄,为一回调函数，带一个参数：form
                alert("提交表单");
                form.submit();   //提交表单
            },

            rules:{
                storageOutCount:{
                    number:true,
                    required:true
                }
            }

        });

    });
</SCRIPT>
<BODY BACKGROUND="${pageContext.request.contextPath}/images/bg.gif">
<FORM NAME="idFrmMain" ID="orderOutDetailAdd" METHOD="POST" ACTION="/admin/stock/addStorageOutDetail.htm" ONSUBMIT="return false">
    <table border="0" width="100%" id="table" cellspacing="0" cellpadding="2" bgcolor="gray">
        <tr>
            <td class="headerbar61" width="15%" colspan="1">出库单详细</td>
            <td class="headerbar63" width="85%" colspan="1">
                <input type="submit" name="save70302002" onClick="return " value=" 保 存 ">&nbsp;
                <input type="button" name="back70302003" onClick="javascript:back()" value=" 返 回 ">
            </td>
        </tr>
    </table>

    <table border=0 cellspacing=0 cellpadding=2 width="100%" height="5">
        <tr>
            <td></td>
        </tr>
    </table>

    <c:if test="${sumList.size() > 0}">
    <table border="0" width="100%" id="table1" cellspacing="1" cellpadding="2" bgcolor="gray">
        <tr>
            <td class="textbar81" width="15%" colspan="1">货号</td>
            <td class="textbar01" width="85%" colspan="1">
                <select name="goodsInfo" style="width:210px" onchange="selectChang(this.value)">
                    <c:forEach items="${sumList}" var="item" varStatus="index">
                        <%--<c:if test="${'' != sumList.get(index.count-1)}">--%>
                            <option value="${item.get(1).goodsID}*${item.get(1).goods_color}*${item.get(1).goods_size}*${item.get(0)}">${item.get(1).goods_number}</option>
                        <%--</c:if>--%>
                    </c:forEach>
                </select></td>
        </tr>
        <tr>
            <td class="textbar81" width="15%" colspan="1">色号</td>
            <td class="textbar01" width="85%" colspan="1">
                <input id="goodsColor" value="${sumList.get(0).get(1).goods_color}" readonly="readonly" style="width:210px;"></td>
        </tr>

        <tr>
            <td class="textbar81" width="15%" colspan="1">尺码</td>
            <td class="textbar01" width="85%" colspan="1">
                <input id="goodsSize" value="${sumList.get(0).get(1).goods_size}" readonly="readonly" style="width:210px;"></td>
        </tr>
        <tr>
            <td class="textbar81" width="15%" colspan="1">库存数量</td>
            <td class="textbar01" width="85%" colspan="1">
                <input id="storageCount" value="${sumList.get(0).get(0)}" readonly="readonly" style="width:210px;"></td>
        </tr>
        <tr>
            <td class="textbar81" width="15%" colspan="1">出库数量</td>
            <td class="textbar01" width="85%" colspan="1">
                <input id="storageOutCount" name="storageOutCount" value="" style="width:210px;"></td>
        </tr>
        <input type="hidden" name="storageID" value="${storageID}" />
        <input type="hidden" name="storageOutID" value="${storageOutID}">
    </table>
    </c:if>
    <c:if test="${sumList.size() == 0}">
        <H1>this storage have not goods at all!~~!</H1>
    </c:if>
    <table border=0 cellspacing=0 cellpadding=0 width="100%" height=5>
        <tr>
            <td></td>
        </tr>
    </table>
</FORM>
</BODY>
</html>




