<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<HTML>
<head>
    <META HTTP-EQUIV="content-type" CONTENT="text/html; charset=gb2312">
    <META HTTP-EQUIV="content-script-type" CONTENT="text/JavaScript">
    <META HTTP-EQUIV="content-style-type" CONTENT="text/css">
    <title>入库单详细</title>
    <link rel="stylesheet" href="${pageContext.request.contextPath}/css/cjpm.css">
    <script type="text/javascript" src="${pageContext.request.contextPath}/js/page.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/js/jquery-1.11.1.min.js"></script>
    <script src="${pageContext.request.contextPath}/js/jquery.validate.min.js"></script>
    <script src="${pageContext.request.contextPath}/js/messages_cn.js"></script>
</head>

<SCRIPT LANGUAGE="javaScript">
    $(function () {
        var validate = $("#orderInDetailAdd").validate({
            debug: true, //调试模式取消submit的默认提交功能
            //errorClass: "label.error", //默认为错误的样式类为：error
            focusInvalid: false, //当为false时，验证无效时，没有焦点响应
            onkeyup: false,
            submitHandler: function (form) {   //表单提交句柄,为一回调函数，带一个参数：form
                alert("提交表单");
                form.submit();   //提交表单
            },

            rules: {
                goodsID: {
                    required: true
                },
                storageInNum: {
                    required: true
                }

            }

        });

    });

    function getOneGood(goodsID) {
//        alert("选中的值"+goodsID);
        $.ajax({
            url: "/admin/stock/getGoodsMessage.htm",
            type: "post",
            data: {
                goodsID: goodsID
            },
            success: function (data) {
                if (data.code == 0) {
                    //获取一个货物的信息并显示
//                    alert(data.goods.goods_name);
                    document.getElementById("goods_name").innerHTML = data.goods.goods_name;
                    document.getElementById("goods_color").innerHTML = data.goods.goods_color;
                    document.getElementById("goods_size").innerHTML = data.goods.goods_size;
                    document.getElementById("factoryPrice").innerHTML = data.goods.factoryPrice;
                    document.getElementById("retailPrice").innerHTML = data.goods.retailPrice;
                    document.getElementById("fabric").innerHTML = data.goods.fabric;
                    document.getElementById("material").innerHTML = data.goods.material;
                }
                if (data.code == 1) {
                    alert(data.message)
                }
            }
        });
    }

</SCRIPT>
<BODY BACKGROUND="${pageContext.request.contextPath}/images/bg.gif">
<FORM NAME="idFrmMain" ID="orderInDetailAdd" METHOD="POST" ACTION="/admin/stock/storageInDetailAdd.htm"
      ONSUBMIT="return true">
    <input type="hidden" name="storageInID" value="${storageInID}"/>
    <input type="hidden" name="storageID" value="${storageID}"/>
    <table border="0" width="100%" id="table1" cellspacing="0" cellpadding="2" bgcolor="gray">
        <tr>
            <td class="headerbar61" width="15%" colspan="1">入库单详细</td>
            <td class="headerbar63" width="85%" colspan="1">
                <input type="submit" name="save70302002" value=" 保 存 ">&nbsp;
                <input type="button" name="back70302003" onClick="javascript:back()" value=" 返 回 ">
            </td>
        </tr>
    </table>

    <table border=0 cellspacing=0 cellpadding=2 width="100%" height="5">
        <tr>
            <td></td>
        </tr>
    </table>

    <table border="0" width="100%" id="table1" cellspacing="1" cellpadding="2" bgcolor="gray">
        <tr>
            <td class="textbar81" width="15%" colspan="1">货号</td>
            <td class="textbar01" width="85%" colspan="1">
                <select name="goodsID" id="goodsID" style="width:210px " onchange="javaScript:getOneGood(this.value)">
                    <option value="" selected="selected">请选择</option>
                    <c:forEach items="${goodsList}" var="item">
                        <option value="${item.goodsID}">${item.goods_number}</option>
                    </c:forEach>
                </select>
        </tr>
        <tr>
            <td class="textbar81" width="15%" colspan="1">名称</td>
            <td class="textbar01" width="85%" colspan="1">
                <p id="goods_name"></p>
            </td>
        </tr>
        <tr>
            <td class="textbar81" width="15%" colspan="1">颜色</td>
            <td class="textbar01" width="85%" colspan="1">
                <p id="goods_color"></p>
            </td>
        </tr>
        <tr>
            <td class="textbar81" width="15%" colspan="1">尺码</td>
            <td class="textbar01" width="85%" colspan="1">
                <p id="goods_size"></p>
            </td>
        </tr>
        <tr>
            <td class="textbar81" width="15%" colspan="1">出厂价</td>
            <td class="textbar01" width="85%" colspan="1">
                <p id="factoryPrice"></p>
            </td>
        </tr>
        <tr>
            <td class="textbar81" width="15%" colspan="1">零售价</td>
            <td class="textbar01" width="85%" colspan="1">
                <p id="retailPrice"></p>
            </td>
        </tr>
        <tr>
            <td class="textbar81" width="15%" colspan="1">面料</td>
            <td class="textbar01" width="85%" colspan="1">
                <p id="fabric"></p>
            </td>
        </tr>
        <tr>
            <td class="textbar81" width="15%" colspan="1">里料</td>
            <td class="textbar01" width="85%" colspan="1">
                <p id="material"></p>
            </td>
        </tr>
        <tr>
            <td class="textbar81" width="15%" colspan="1">数量</td>
            <td class="textbar01" width="85%" colspan="1"><input name="storageInNum" id="storageInNum"
                                                                 style="width:210px;"/></td>
        </tr>
    </table>
</FORM>
</BODY>
</html>




