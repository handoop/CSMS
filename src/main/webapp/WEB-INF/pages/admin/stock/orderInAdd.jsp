<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<HTML>
<head>
    <META HTTP-EQUIV="content-type" CONTENT="text/html; charset=GB2312">
    <META HTTP-EQUIV="content-script-type" CONTENT="text/JavaScript">
    <META HTTP-EQUIV="content-style-type" CONTENT="text/css">
    <title>采购入库单详细</title>
    <link rel="stylesheet" href="${pageContext.request.contextPath}/css/cjpm.css">
    <script type="text/javascript" src="${pageContext.request.contextPath}/js/page.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/js/laydate.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/js/jquery-1.11.1.min.js"></script>
    <script src="${pageContext.request.contextPath}/js/jquery.validate.min.js"></script>
    <script src="${pageContext.request.contextPath}/js/messages_cn.js"></script>
</head>
<script language="javascript">
    var CalendarWebControl = new atCalendarControl();
</script>
<SCRIPT LANGUAGE="javaScript">

    var trFlag = 0;
    function tabMove0(objId, position) {
        if (event.keyCode == 13) {
            document.getElementById(objId).childNodes[2].innerHTML = '07长面包新款';
            document.getElementById(objId).childNodes[3].innerHTML = '045';
            tabMove(objId, position);
        }
    }
    function goto(strURL) {
        document.forms[0].action = strURL;
        document.forms[0].submit();
    }

    function save() {
        alert('保存成功');
    }
    function setValue() {
        document.forms[0].gys.value = "610";
    }
    function delCom(id) {
        if (id == "1") {
            document.forms[0].gys.value = "";
        } else {
            document.forms[0].sccj.value = "";
        }
    }
    function setValue1() {
        document.forms[0].sccj.value = "";
    }

    $(function(){
        var validate = $("#orderInAdd").validate({
            debug: true, //调试模式取消submit的默认提交功能
            //errorClass: "label.error", //默认为错误的样式类为：error
            focusInvalid: false, //当为false时，验证无效时，没有焦点响应
            onkeyup: false,
            submitHandler: function(form){   //表单提交句柄,为一回调函数，带一个参数：form
                alert("提交表单");
                form.submit();   //提交表单
            },

            rules:{
                storageIn_dateTime:{
                    required:true
                },
                storageID:{
                    required:true
                },
                source_company:{
                    required:true
                },
                receiver_man:{
                    required:true
                }

            }

        });

    });

</SCRIPT>

<BODY BACKGROUND="${pageContext.request.contextPath}/images/bg.gif">
<FORM NAME="mig0101" ID="orderInAdd" METHOD="POST" ACTION="/admin/stock/addStorageInOrder.htm">

    <table border=0 cellspacing=0 cellpadding=2 width="100%" bgcolor="gray">
        <tr>
            <td class="headerbar61">入库单详细</td>
            <td class="headerbar61"><p align="right">
                <input type="submit" value=" 保 存 " onClick="JavaScript:history.save();">
                <input type=button value=" 返 回 " onClick="JavaScript:history.back();">
            </p></td>
        </tr>
    </table>
    <table border=0 cellspacing=1 cellpadding=2 width="100%" bgcolor="gray">
        <tr>
            <td class="textbar81" width="15%">单据号</td>
            <td class="textbar01" width="35%">
                <input type="text" name="storageIn_number" value="${storageIn_number}" readonly size="20">
            </td>
            <td class="textbar81" width="15%">入库日期</td>
            <td class="textbar01" width="35%">
                <input type="text" placeholder="请输入日期" name="storageIn_dateTime" id="storageIn_dateTime" class="laydate-icon"
                       onclick="laydate()"/>
            </td>
        </tr>
        <tr>
            <td class="textbar81" width="20%">所入仓库</td>
            <td class="textbar01" width="30%">
                <select name="storageID" id="storageID" style="width:152px">
                    <option value="">---请选择---</option>
                    <c:forEach items="${storageList}" var="storage">
                        <option value="${storage.id}">${storage.sto_name}</option>
                    </c:forEach>
                </select>
            </td>
            <td class="textbar81" width="20%">来源</td>
            <td class="textbar01" width="30%">
                <input type="text" name="source_company" id="source_company" value="" size="20">
            </td>
        </tr>
        <tr>
            <td class="textbar81" width="15%">经办人</td>
            <td class="textbar01" width="35%">
                <input type="text" name="receiver_man" id="receiver_man" value="" size="20">
            </td>
        </tr>
        <tr>
            <td class="textbar81" width="15%">备注</td>
            <td class="textbar01" width="85%" colspan="3">
                <textarea name="storageIn_note" id="storageIn_note" cols="80" rows="4"></textarea>
            </td>

        </tr>
    </table>

</FORM>
</BODY>
</html>

