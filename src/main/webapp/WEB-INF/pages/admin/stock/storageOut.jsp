<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<HTML>
<head>
<META HTTP-EQUIV="content-type" CONTENT="text/html; charset=GB2312">
<META HTTP-EQUIV="content-script-type" CONTENT="text/JavaScript">
<META HTTP-EQUIV="content-style-type" CONTENT="text/css">
<title>出库单查询</title>
<link rel="stylesheet" href="${pageContext.request.contextPath}/css/cjpm.css">
<script type="text/javascript" src="${pageContext.request.contextPath}/js/cjcalendar.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/js/page.js"></script>
	<script type="text/javascript" src="${pageContext.request.contextPath}/js/jquery-1.11.1.min.js"></script>
	<script type="text/javascript" src="${pageContext.request.contextPath}/js/laydate.js"></script>
</head>
<script language="javascript">
	var CalendarWebControl = new atCalendarControl();
</script>
<SCRIPT LANGUAGE="javaScript">
<!--

function goSearch()
{
	document.forms[0].action="/admin/stock/showStorageOut.htm";
	document.forms[0].submit();
}

function delCom(id){
	if(id == '1'){
		document.idFrmMain.gys.value = "";		
	}else{
		document.idFrmMain.sccj.value = "";
	}
}

function changesize(id){
	document.forms[0].action="storageOut.htm";
	document.forms[0].submit();
}

function jump(id){
	document.forms[0].action="storageOut.htm";
	document.forms[0].submit();	
}

function locatePage(id){
	document.forms[0].action="storageOut.htm";
	document.forms[0].submit();		
}

function setValue()
{
	document.all.sccj.value="青岛雪中飞贸易有限公司";
}
-->
function del(storageOutID) {
	if (confirm("您确定删除该条数据？")) {
		$.ajax({
			url: "/admin/stock/deleteStorageOut.htm",
			type: "post",
			data: {
				storageOutID: storageOutID
			},
			success: function (data) {
				if (data.code == 0) {
					alert("delete successful!")
					window.location.reload()
				}
				if (data.code == 1) {
					alert(data.message)
				}
			}
		});
	}
}
</SCRIPT>

<BODY BACKGROUND="${pageContext.request.contextPath}/images/bg.gif">
<FORM NAME="idFrmMain" ID="idmig0101" METHOD="POST"  ACTION="" ONSUBMIT="return false" >
<input type="hidden" id="slide_img">
<table border=0 cellspacing=0 cellpadding=2 width="100%" bgcolor="gray">
<tr>
	<td class="headerbar61">出库单查询</td>
	<td class="headerbar61"><p align="right">		
		<input type=submit value=" 查 询 " onClick="JavaScript:goSearch();"></p></td>
</tr>
</table>


<table border=0 cellspacing=0 cellpadding=2 width="100%" height="5">
<tr>
	<td></td>
</tr>
</table>

<table border=0 cellspacing=1 cellpadding=2 width="100%" bgcolor="gray">	
	<tr>
		<td class= "textbar81" width="15%">所出仓库</td>
		<td class="textbar01" width="35%">
			<select name="storageID" style="width:200px">
				<option value="" selected>------</option>
				<c:forEach var="storage" items="${storageList}">
					<option value="${storage.id}">${storage.sto_name}</option>
				</c:forEach>
			</select>
		</td>
	  <td class="textbar81" width="15%">单据编号</td>
		<td class="textbar01" width="35%">
			<input type="text" name="storageOutNumber" style="width:152px">
		</td>				

	</tr>
	
	<tr>			  
		<td class="textbar81" width="15%">出库日期</td>
		<td class="textbar01" width="35%" colspan="3">
			<%--<input type="text" name="frmWRPT_OPT_DATE2_PJT70302" id="frmWRPT_OPT_DATE2_PJT70302" value="2007-06-21" readonly="readonly" size="12">--%>
			<%--<input type="image" src="${pageContext.request.contextPath}/images/calendar.gif" width="18" height="17" onClick="CalendarWebControl.show(forms[0].frmWRPT_OPT_DATE2_PJT70302,'',forms[0].frmWRPT_OPT_DATE2_PJT70302);" title="显示日历" />--%>
				<input name="startTime" placeholder="请输入日期" class="laydate-icon" onclick="laydate()"  />
			~ 
			<%--<input type="text" name="frmWRPT_OPT_DATE3_PJT70302" id="frmWRPT_OPT_DATE3_PJT70302" value="2007-06-26" readonly="readonly" size="12">--%>
			<input name="endTime" placeholder="请输入日期" class="laydate-icon" onclick="laydate()"  />
			<%--<input type="image" src="${pageContext.request.contextPath}/images/calendar.gif" width="18" height="17" onClick="CalendarWebControl.show(forms[0].frmWRPT_OPT_DATE3_PJT70302,'',forms[0].frmWRPT_OPT_DATE3_PJT70302);" title="显示日历" />--%>
	  </td>
		
	</tr>	
	
</table>

<table border=0 cellspacing=0 cellpadding=0 width="100%" height=5>
<tr>
	<td></td>
</tr>
</table>

<table border="0" width="100%" id="table" cellspacing="0"  cellpadding="2"  bgcolor="gray">
	<tr>
  	<td class="headerbar61" width="100%" colspan="1">出库单明细</td>
		<td class="headerbar61"><p align="right">		
			<input type=submit value=" 新 增 " onClick="JavaScript:goto('${pageContext.request.contextPath}/admin/stock/storageOutAdd.htm');">
		</td>
</table>
<table border="0" width="100%" id="table1" cellspacing="0"  cellpadding="0"  bgcolor="gray">
	<tr>
  	<td  width="100%" colspan="1">
    	<table  border="0" cellspacing="1" cellpadding="2" width="100%">
				<tr>
					<td  width="5%"  class="headerbar82">序号</td>
					<td   width="15%"  class="headerbar82">单据编号</td>
					<td   width="20%" class="headerbar82">所出仓库</td>
					<td    width="15%" class="headerbar82">出库日期</td>					
					<td    width="15%" class="headerbar82">接收人</td>
					<td    width="25%" class="headerbar82">发往地址</td>
					<td  class="headerbar82">操作</td>			
				</tr>
			<c:forEach var="storageOut" items="${page.recordList}" varStatus="str">
				<tr>
					<td  class="gridbar11" align="center">${str.count}</td>
					<td  class="gridbar11" align="center">
						<a href="${pageContext.request.contextPath}/admin/stock/storageOutDetail.htm?storageOutID=${storageOut.storageOutID}">${storageOut.storageOutNumber}</a>
					</td>
					<td  class="gridbar11" align="center">${storageOut.storage.sto_name}</td>
					<td  class="gridbar11" align="center">${storageOut.storageOutDateTime}</td>
					<td  class="gridbar11" align="left">${storageOut.receiverName}</td>
					<td  class="gridbar11" align="left">${storageOut.receiverAddress}</td>
					<td  class="gridbar11" align="center">
						<a href = "#">
							<img src="${pageContext.request.contextPath}/images/del.gif" align="bottom" border="0" alt="作废"
								 onClick="javascript:del(${storageOut.storageOutID})" />
						</a>
					</td>
				</tr>
			</c:forEach>
			</table>
	  </td>
	</tr>
</table>
 

<table width="100%" border="0" cellpadding="0" cellspacing="2">
	<tr>
  	<td colspan="2" align="right" height="20"  nowrap class="textbar3" >
		&nbsp; 共${page.recordCount}条 &nbsp; 第${page.currentPage}/${page.pageCount}页 &nbsp;
		<c:if test="${page.currentPage>2}">
			<a style="cursor:hand"
			   href="${pageContext.request.contextPath}/admin/stock/showStorageOut.htm?pageNum=1&storageID=${storageID}&storageOutNumber=${storageOutNumber}&startTime=${startTime}&endTime=${endTime}">首页</a>&nbsp;
		</c:if>
		<c:if test="${page.currentPage>1}">
			<a style="cursor:hand"
			   href="${pageContext.request.contextPath}/admin/stock/showStorageOut.htm?pageNum=${page.currentPage-1}&storageID=${storageID}&storageOutNumber=${storageOutNumber}&startTime=${startTime}&endTime=${endTime}">上一页</a>&nbsp;
		</c:if>
		<c:forEach begin="${page.beginIndex}" end="${page.endIndex}" varStatus="step">
			<a style="cursor:hand"
			   href="${pageContext.request.contextPath}/admin/stock/showStorageOut.htm?pageNum=${step.index}&storageID=${storageID}&storageOutNumber=${storageOutNumber}&startTime=${startTime}&endTime=${endTime}">${step.index}</a>&nbsp;
		</c:forEach>
		<c:if test="${page.currentPage<page.pageCount}">
			<a style="cursor:hand"
			   href="${pageContext.request.contextPath}/admin/stock/showStorageOut.htm?pageNum=${page.currentPage+1}&storageID=${storageID}&storageOutNumber=${storageOutNumber}&startTime=${startTime}&endTime=${endTime}">下一页</a>&nbsp;
		</c:if>
		<c:if test="${page.currentPage+1<page.pageCount}">
			<a style="cursor:hand"
			   href="${pageContext.request.contextPath}/admin/stock/showStorageOut.htm?pageNum=${page.pageCount}&storageID=${storageID}&storageOutNumber=${storageOutNumber}&startTime=${startTime}&endTime=${endTime}">尾页</a>&nbsp;
		</c:if>
	  </td>
  </tr>
</table>
</FORM>
</BODY>
</html>

