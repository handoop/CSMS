<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<HTML>
<head>
    <META HTTP-EQUIV="content-type" CONTENT="text/html; charset=GB2312">
    <META HTTP-EQUIV="content-script-type" CONTENT="text/JavaScript">
    <META HTTP-EQUIV="content-style-type" CONTENT="text/css">
    <title>采购入库管理</title>
    <link rel="stylesheet" href="${pageContext.request.contextPath}/css/cjpm.css">
    <script type="text/javascript" src="${pageContext.request.contextPath}/js/cjcalendar.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/js/page.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/js/jquery-1.11.1.min.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/js/laydate.js"></script>
</head>
<script language="javascript">
    var CalendarWebControl = new atCalendarControl();
</script>
<SCRIPT LANGUAGE="javaScript">
    <!--

    function goSearch() {
        document.forms[0].action = "storageIn.htm";
        document.forms[0].submit();
    }

    function delCom(id) {
        if (id == '1') {
            document.idFrmMain.gys.value = "";
        } else {
            document.idFrmMain.sccj.value = "";
        }
    }

    function changesize(id) {
        document.forms[0].action = "storageIn.htm";
        document.forms[0].submit();
    }

    function jump(id) {
        document.forms[0].action = "storageIn.htm";
        document.forms[0].submit();
    }

    function locatePage(id) {
        document.forms[0].action = "storageIn.htm";
        document.forms[0].submit();
    }


    -->
    function del(storageInID) {
        if (confirm("您确定删除该条数据")) {
            $.ajax({
                url: "/admin/stock/delStorageInOrder.htm",
                type: "post",
                data: {
                    storageInID: storageInID
                },
                success: function (data) {
                    if (data.code == 0) {
                        alert("delete successful!")
                        window.location.reload()
                    }
                    if (data.code == 1) {
                        alert(data.message)
                    }
                    if(data.code==2){
                        alert(data.message)
                    }
                }
            });
        }
    }


</SCRIPT>

<BODY BACKGROUND="${pageContext.request.contextPath}/images/bg.gif">
<FORM NAME="idFrmMain" ID="idmig0101" METHOD="POST" ACTION="/admin/stock/showStorageIn.htm" ONSUBMIT="return true">

    <table border=0 cellspacing=0 cellpadding=2 width="100%" bgcolor="gray">
        <tr>
            <td class="headerbar61">入库单查询</td>
            <td class="headerbar61"><p align="right">
                <input type="submit" value=" 查 询 ">
            </p>
            </td>
        </tr>
    </table>


    <table border=0 cellspacing=0 cellpadding=2 width="100%" height="5">
        <tr>
            <td></td>
        </tr>
    </table>


    <table border=0 cellspacing=1 cellpadding=2 width="100%" bgcolor="gray">
        <tr>
            <td class="textbar81" width="15%">所入仓库</td>
            <td class="textbar01" width="35%">
                <select name="storageID" style="width:152px">
                    <option value="">---请选择---</option>
                    <c:forEach items="${storageList}" var="storage">
                        <option value="${storage.id}">${storage.sto_name}</option>
                    </c:forEach>
                </select>
            </td>
            <td class="textbar81" width="15%">单据编号</td>
            <td class="textbar01" width="35%">
                <input type="text" name="storageIn_number" style="width:152px">
            </td>

        </tr>

        <tr>
            <td class="textbar81" width="15%">入库日期</td>
            <td class="textbar01" width="35%" colspan="3">
                <input type="text" placeholder="请输入日期" name="storageIn_dateStartTime" class="laydate-icon"
                       onclick="laydate()"/>
                ~
                <input type="text" placeholder="请输入日期" name="storageIn_dateEndTime" class="laydate-icon"
                       onclick="laydate()"/>
            </td>

        </tr>

    </table>
    <table border=0 cellspacing=0 cellpadding=0 width="100%" height=5>
        <tr>
            <td></td>
        </tr>
    </table>

    <table border="0" width="100%" id="table1" cellspacing="0" cellpadding="2" bgcolor="gray">
        <tr>
            <td class="headerbar61" width="100%" colspan="1">入库单明细</td>
            <td class="headerbar61">
                <p align="rigth">
                    <input type=submit value=" 新 增 " onClick="JavaScript:goto('showOrderInAdd.htm');">
                </p>
            </td>
    </table>

    <table border="0" width="100%" id="table1" cellspacing="0" cellpadding="0" bgcolor="gray">
        <tr>
            <td width="100%" colspan="1">
                <table border="0" cellspacing="1" cellpadding="2" width="100%">
                    <tr>
                        <td width="5%" class="headerbar82">序号</td>
                        <td width="15%" class="headerbar82">单据编号</td>
                        <td width="20%" class="headerbar82">所入仓库</td>
                        <td width="15%" class="headerbar82">入库日期</td>
                        <td width="15%" class="headerbar82">经办人</td>
                        <td width="25%" class="headerbar82">来源</td>
                        <td class="headerbar82">操作</td>
                    </tr>
                    <c:forEach items="${storageInPage.recordList}" var="item" varStatus="index">
                        <tr>
                            <td class="gridbar11" align="center">${index.index+1}</td>
                            <td class="gridbar11" align="center"><a
                                    href="${pageContext.request.contextPath}/admin/stock/showStorageInDetail.htm?id=${item.storageInID}">${item.storageIn_number}</a>
                            </td>
                            <td class="gridbar11" align="center">${item.storage.sto_number}</td>
                            <td class="gridbar11" align="center">${item.storageIn_dateTime}</td>
                            <td class="gridbar11" align="left">${item.receiver_man}</td>
                            <td class="gridbar11" align="left">${item.source_company}</td>
                            <td class="gridbar11" align="center">
                                <a href="#"><img src="${pageContext.request.contextPath}/images/del.gif" align="bottom"
                                                 border="0" alt="作废" onClick="javascript:del(${item.storageInID})"/></a>
                            </td>
                        </tr>
                    </c:forEach>


                </table>
            </td>
        </tr>
    </table>

    <table width="100%" border="0" cellpadding="0" cellspacing="2">
        <tr>
            <td colspan="2" align="right" height="20" nowrap class="textbar3">
                &nbsp; ${storageInPage.pageCount} &nbsp;&nbsp; 第${storageInPage.currentPage}/${storageInPage.pageCount}页
                &nbsp;&nbsp;
                <c:if test="${storageInPage.currentPage>2}">
                    <a style="cursor:hand"
                       href="${pageContext.request.contextPath}/admin/stock/showStorageIn.htm?pageNum=1&storageID=${storageID}&storageIn_number=${storageIn_number}
                       &storageIn_dateStartTime=${storageIn_dateStartTime}&storageIn_dateEndTime=${storageIn_dateEndTime}">首页</a>&nbsp;
                </c:if>
                <c:if test="${storageInPage.currentPage>1}">
                    <a style="cursor:hand"
                       href="${pageContext.request.contextPath}/admin/stock/showStorageIn.htm?pageNum=${storageInPage.currentPage-1}&storageID=${storageID}&storageIn_number=${storageIn_number}
                       &storageIn_dateStartTime=${storageIn_dateStartTime}&storageIn_dateEndTime=${storageIn_dateEndTime}">上一页</a>&nbsp;
                </c:if>
                <c:forEach begin="${storageInPage.beginIndex}" end="${storageInPage.endIndex}" varStatus="step">
                    <a style="cursor:hand"
                       href="${pageContext.request.contextPath}/admin/stock/showStorageIn.htm?pageNum=${step.index}&storageID=${storageID}&storageIn_number=${storageIn_number}
                       &storageIn_dateStartTime=${storageIn_dateStartTime}&storageIn_dateEndTime=${storageIn_dateEndTime}">${step.index}</a>&nbsp;
                </c:forEach>
                <c:if test="${storageInPage.currentPage<storageInPage.pageCount}">
                    <a style="cursor:hand"
                       href="${pageContext.request.contextPath}/admin/stock/showStorageIn.htm?pageNum=${storageInPage.currentPage+1}&storageID=${storageID}&storageIn_number=${storageIn_number}
                       &storageIn_dateStartTime=${storageIn_dateStartTime}&storageIn_dateEndTime=${storageIn_dateEndTime}">下一页</a>&nbsp;
                </c:if>
                <c:if test="${storageInPage.currentPage+1<storageInPage.pageCount}">
                    <a style="cursor:hand"
                       href="${pageContext.request.contextPath}/admin/stock/showStorageIn.htm?pageNum=${storageInPage.pageCount}&storageID=${storageID}&storageIn_number=${storageIn_number}
                       &storageIn_dateStartTime=${storageIn_dateStartTime}&storageIn_dateEndTime=${storageIn_dateEndTime}">尾页</a>&nbsp;s
                </c:if>
            </td>
        </tr>
    </table>
</FORM>
</BODY>
</html>

