<%@ taglib prefix="jstl" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<HTML>
<head>
    <META HTTP-EQUIV="content-type" CONTENT="text/html; charset=GB2312">
    <META HTTP-EQUIV="content-script-type" CONTENT="text/JavaScript">
    <META HTTP-EQUIV="content-style-type" CONTENT="text/css">
    <title>用户管理</title>
    <link rel="stylesheet" href="${pageContext.request.contextPath}/css/cjpm.css">
    <script type="text/javascript" src="${pageContext.request.contextPath}/js/jquery-1.11.1.min.js"></script>
</head>

<script type="text/javascript">
    function goto() {
        document.forms[0].action = "/admin/user/register.htm";
        document.forms[0].method = "GET";
        document.forms[0].submit();
    }

    function del(id) {
        if (confirm("您确定删除该条数据？")) {
            $.ajax({
                url: "/admin/user/delete.htm",
                type: "post",
                data: {id: id},
                success: function (data) {
                    if (data.code == 0) {
                        alert("delete successful!")
                        window.location.reload()
                    }
                    if (data.code == 1) {
                        alert(data.message)
                    }
                }
            });
        }
    }

    function goSearch() {
        document.forms[0].action = "/admin/user/list.htm";
        document.forms[0].submit();
    }

</script>

<BODY BACKGROUND="${pageContext.request.contextPath}/images/bg.gif">
<FORM NAME="mig0101" ID="idmig0101" METHOD="POST" ACTION="" ONSUBMIT="return false">
    <table border=0 cellspacing=0 cellpadding=2 width="100%" bgcolor="gray">
        <tr>
            <td class="headerbar61">用户明细查询</td>
            <td class="headerbar61"><p align="right"><input type=submit value=" 查 询 " onClick="goSearch();"></p></td>
        </tr>
    </table>

    <table border=0 cellspacing=0 cellpadding=2 width="100%" height="5">
        <tr>
            <td></td>
        </tr>
    </table>
    <table border=0 cellspacing=1 cellpadding=2 width="100%" bgcolor="gray">
        <tr>
            <td class="textbar81" width="15%">用户姓名</td>
            <td class="textbar01" width="35%">
                <input type="text" name="name" value="${name}" size="20">
            </td>
            <td class="textbar81" width="15%">用户登录号</td>
            <td class="textbar01" width="35%"><input type="text" name="username" value="${username}" size="20"></td>
        </tr>

    </table>
    <table border=0 cellspacing=0 cellpadding=2 width="100%" height="5">
        <tr>
            <td></td>
        </tr>
    </table>
    <table border="0" width="100%" id="table1" cellspacing="0" cellpadding="2" bgcolor="gray">
        <tr>
            <td class="headerbar61" width="50%" colspan="1">用户明细表</td>
            <td class="headerbar63" width="50%" colspan="1">
                <input type="button" name="add" tabindex="1" onClick="javascript:goto()" value=" 新 增 "></td>
        </tr>
    </table>

    <table border=0 cellspacing=0 cellpadding=2 width="100%" height="5">
        <tr>
            <td></td>
        </tr>
    </table>

    <table border="0" width="100%" id="table1" cellspacing="0" cellpadding="0" bgcolor="gray">
        <tr>
            <td width="100%" colspan="1">
                <table border="0" cellspacing="1" cellpadding="2" width="100%">
                    <tr>
                        <td width="5%" class="headerbar82">序号</td>
                        <td width="35%" class="headerbar82">用户登录号</td>
                        <td width="35%" class="headerbar82">用户姓名</td>
                        <td class="headerbar82">操作</td>
                    </tr>
                    <jstl:forEach items="${page.recordList}" var="item" varStatus="index">
                        <tr>
                            <td class="gridbar11" align="center">${index.index+1}</td>
                            <td class="gridbar11" align="center"><a href="${pageContext.request.contextPath}/admin/user/register.htm?username=${item.username}">${item.username}</a></td>
                            <td class="gridbar11" align="center">${item.name}</td>
                            <td class="gridbar11" align="center">
                                <a href="javascript:;"><img src="${pageContext.request.contextPath}/images/del.gif" align="bottom"
                                                 border="0" alt="删除" onClick="del(${item.id})"/></a></td>
                        </tr>
                    </jstl:forEach>
                </table>
            </td>
        </tr>
    </table>


    <table width="100%" border="0" cellpadding="1" cellspacing="2">
        <tr>
            <td colspan="2" align="right" height="20" nowrap class="textbar3">
                &nbsp; 共${page.recordCount}条 &nbsp; 第${page.currentPage}/${page.pageCount}页 &nbsp;
                <jstl:if test="${page.currentPage>2}">
                    <a style="cursor:hand"
                       href="${pageContext.request.contextPath}/admin/user/list.htm?pageNum=1&name=${name}&username=${username}">首页</a>&nbsp;
                </jstl:if>
                <jstl:if test="${page.currentPage>1}">
                    <a style="cursor:hand"
                       href="${pageContext.request.contextPath}/admin/user/list.htm?pageNum=${page.currentPage-1}&name=${name}&username=${username}">上一页</a>&nbsp;
                </jstl:if>
                <jstl:forEach begin="${page.beginIndex}" end="${page.endIndex}" varStatus="step">
                    <a style="cursor:hand"
                       href="${pageContext.request.contextPath}/admin/user/list.htm?pageNum=${step.index}&name=${name}&username=${username}">${step.index}</a>&nbsp;
                </jstl:forEach>
                <jstl:if test="${page.currentPage<page.pageCount}">
                    <a style="cursor:hand"
                       href="${pageContext.request.contextPath}/admin/user/list.htm?pageNum=${page.currentPage+1}&name=${name}&username=${username}">下一页</a>&nbsp;
                </jstl:if>
                <jstl:if test="${page.currentPage+1<page.pageCount}">
                    <a style="cursor:hand"
                       href="${pageContext.request.contextPath}/admin/user/list.htm?pageNum=${page.pageCount}&name=${name}&username=${username}">尾页</a>&nbsp;
                </jstl:if>
            </td>
            </ul>
        </tr>
    </table>


</FORM>
</BODY>
</html>

