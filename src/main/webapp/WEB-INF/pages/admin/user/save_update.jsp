<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib prefix="mvc" uri="http://www.springframework.org/tags/form" %>
<HTML>
<head>
    <META HTTP-EQUIV="content-type" CONTENT="text/html; charset=gb2312">
    <META HTTP-EQUIV="content-script-type" CONTENT="text/JavaScript">
    <META HTTP-EQUIV="content-style-type" CONTENT="text/css">
    <title>用户详细</title>
    <link rel="stylesheet" href="${pageContext.request.contextPath}/css/cjpm.css">
    <script type="text/javascript" src="${pageContext.request.contextPath}/js/jquery-1.11.1.min.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/js/jquery.md5.js"></script>
</head>

<SCRIPT LANGUAGE="javaScript">
    <!--
    function save() {
        alert("保存成功！");
    }

    function back() {
        history.back();
    }

    -->
</SCRIPT>
<BODY BACKGROUND="../image/bg.gif">
<mvc:form name="idFrmMain" id="_form" method="POST" action="/admin/user/register.htm" modelAttribute="user" onsubmit="submit_form()">
    <table border="0" width="100%" id="table1" cellspacing="0" cellpadding="2" bgcolor="gray">
        <tr>
            <td class="headerbar61" width="15%" colspan="1">用户详细</td>
            <td class="headerbar63" width="85%" colspan="1">
                <input type="submit" name="save70302002" id="_submit" value=" 保 存 ">&nbsp;
                <input type="button" name="back70302003" onClick="javascript:back()" value=" 返 回 " >
            </td>
        </tr>
    </table>

    <table border=0 cellspacing=0 cellpadding=2 width="100%" height="5">
        <tr>
            <td></td>
        </tr>
    </table>

    <table border="0" width="100%" id="table1" cellspacing="1" cellpadding="2" bgcolor="gray">

        <mvc:hidden path="id"/>

        <tr>
            <td class="textbar81" width="15%" colspan="1">用户登录号</td>
            <td class="textbar01" width="85%" colspan="1"><mvc:input path="username" size="20" required="必填"/></td>
        </tr>
        <tr>
            <td class="textbar81" width="15%" colspan="1">用户姓名</td>
            <td class="textbar01" width="85%" colspan="1"><mvc:input path="name" size="20" required="必填"/></td>
        </tr>
        <tr>
            <td class="textbar81" width="15%" colspan="1">用户密码</td>
            <td class="textbar01" width="85%" colspan="1"><mvc:input path="password" type="password" required="必填"/></td>
        </tr>
        <tr>
            <td class="textbar81" width="15%" colspan="1">用户简介</td>
            <td class="textbar01" width="85%" colspan="1"><mvc:textarea path="summary"/>
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <label style="color:red; font-size: 11px;">
                    <mvc:errors path="*"/>
                </label>
            </td>
        </tr>


    </table>

    <table border=0 cellspacing=0 cellpadding=0 width="100%" height=5>
        <tr>
            <td></td>
        </tr>
    </table>
</mvc:form>
<script type="text/javascript">
    function submit_form(){
        var form = document.forms._form
        var password = form.password.value
        form.password.value = $.md5(password)
        form.submit()
    }
</script>
</BODY>
</html>




