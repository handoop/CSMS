<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=gb2312">
    <title>在线通知</title>
    <link rel="stylesheet" href="${pageContext.request.contextPath}/css/cjpm.css">
    <script type="text/javascript" src="${pageContext.request.contextPath}/js/page.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/js/msn_message.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/js/jquery-1.11.1.min.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/js/jquery.md5.js"></script>
    <style type="text/css">
        <!--
        body {
            background-color: #f8f7f7;
        }

        -->
    </style>
</head>

<body BACKGROUND="${pageContext.request.contextPath}/images/bg.gif">

<form name="update_form" id="_form" method="POST" action="" onsubmit="return false">
    <table border=0 cellspacing=0 cellpadding=2 width="100%" bgcolor="gray">
        <tr>
            <td class="headerbar61"> 修改密码</td>
            <td class="headerbar61">
                <p align="right">
                    <input id="_update" type="button" value=" 保 存 ">
                </p>
            </td>
        </tr>
    </table>
    <table border=0 cellspacing=0 cellpadding=2 width="100%" height="5">
        <tr>
            <td></td>
        </tr>
    </table>
    <table border=0 cellspacing=1 cellpadding=2 width="100%" bgcolor="gray">
        <tr>
            <td class="textbar81" width="15%">旧密码</td>
            <td class="textbar01" width="85%"><input type="password" name="old_password" size="30" required></td>
        </tr>
        <tr>
            <td class="textbar81" width="15%">新密码</td>
            <td class="textbar01" width="85%"><input type="password" name="new_password" size="30" required></td>
        </tr>
        <tr>
            <td class="textbar81" width="15%">确认密码</td>
            <td class="textbar01" width="85%"><input type="password" name="confirm_new_password" size="30" required>
            </td>
        </tr>
    </table>

</form>
<script type="text/javascript">
    $('#_update').click(function () {
        var form = document.forms._form
        var old_password = form.old_password.value
        var new_password = form.new_password.value
        var confirm_new_password = form.confirm_new_password.value
        if(!old_password || !new_password || !confirm_new_password){
            alert('密码不能为空')
            return
        }
        if(new_password!=confirm_new_password){
            alert('两次密码不一致')
            return
        }
        console.log('old_password = ' + old_password)
        console.log('new_password = ' + new_password)
        console.log('confirm_new_password = ' + confirm_new_password)
        if (new_password === confirm_new_password) {
            form.old_password.value = $.md5(old_password)
            form.new_password.value = $.md5(new_password)
            form.confirm_new_password.value = $.md5(confirm_new_password)
            $.ajax({
                type: 'POST',
                url: '/admin/user/update/password.htm',
                data: $('#_form').serialize(),
                success: function (args) {
                    if (args.code == '1') {
                        alert(args.message)
                    } else if (args.code == '0') {
                        alert(args.message)
                        window.location.href = '/admin/user/list.htm'
                    }
                }
            })
        } else {
            alert('两次密码不一致')
        }
    })
</script>
</body>
</html>


