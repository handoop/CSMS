package org.victoria.common.front.helper;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.victoria.common.utils.StringUtils;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by handoop on 2015/3/21.
 */
@Component
public class WebHelper {

    public String filterHTML(String content){
        if (StringUtils.isEmpty(content))
            return null;
        int length = content.length();
        char[] chars = new char[length];
        content.getChars(0, length, chars, 0);
        StringBuilder result = new StringBuilder(length + 50);
        for (int i = 0; i < length; i++) {
            switch (chars[i]) {
                case '<':
                    result.append("&lt;");
                    break;
                case '>':
                    result.append("&gt;");
                    break;
                case '&':
                    result.append("&amp;");
                    break;
                case '"':
                    result.append("&quot;");
                    break;
                default:
                    result.append(chars[i]);
            }
        }
        return result.toString();
    }

}
