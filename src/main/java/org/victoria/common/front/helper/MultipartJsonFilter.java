package org.victoria.common.front.helper;

import com.alibaba.fastjson.serializer.JSONSerializer;
import com.alibaba.fastjson.serializer.PropertyPreFilter;
import com.alibaba.fastjson.serializer.SimplePropertyPreFilter;

import java.util.ArrayList;

/**
 * Created by handoop on 2015/4/26.
 */
public class MultipartJsonFilter extends SimplePropertyPreFilter {

    private ArrayList<PropertyPreFilter> filters;

    public MultipartJsonFilter(){
        filters = new ArrayList();
    }

    public MultipartJsonFilter setFilter(PropertyPreFilter filter){
        filters.add(filter);
        return this;
    }

    @Override
    public boolean apply(JSONSerializer serializer, Object source, String name) {
        for (PropertyPreFilter filter : filters){
            boolean isOk = filter.apply(serializer, source, name);
            if (!isOk)
                return false;
        }
        return super.apply(serializer, source, name);
    }
}
