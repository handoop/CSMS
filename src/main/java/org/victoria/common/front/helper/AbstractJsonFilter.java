package org.victoria.common.front.helper;

import com.alibaba.fastjson.serializer.JSONSerializer;
import com.alibaba.fastjson.serializer.SimplePropertyPreFilter;
import org.hibernate.proxy.pojo.javassist.JavassistLazyInitializer;

import javax.persistence.Entity;
import javax.persistence.MappedSuperclass;
import java.lang.reflect.Method;

/**
 * Created by handoop on 2015/4/26.
 */
public abstract class AbstractJsonFilter extends SimplePropertyPreFilter{

    public Class clazz;

    public abstract boolean doSomething(JSONSerializer serializer, Object source, String name);

    @Override
    public boolean apply(JSONSerializer serializer, Object source, String name) {
        clazz = source.getClass();
        try {
            Method m = clazz.getDeclaredMethod("getHandler");
            JavassistLazyInitializer initializer = (JavassistLazyInitializer) m.invoke(source, null);
            clazz = initializer.getPersistentClass();
        } catch (Exception e) { }

        if (clazz.getAnnotation(Entity.class) != null || clazz.getAnnotation(MappedSuperclass.class)!=null){

            return doSomething(serializer, source, name);

        }else
            return super.apply(serializer, source, name);
    }
}
