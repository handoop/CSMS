package org.victoria.common.front.helper;

import com.alibaba.fastjson.serializer.JSONSerializer;
import org.victoria.common.annotation.JsonSubString;
import org.victoria.common.utils.FrontUtils;

import java.lang.reflect.Method;

/**
 * Created by handoop on 2015/4/26.
 */
public class SubStringFilter extends AbstractJsonFilter{

    @Override
    public boolean doSomething(JSONSerializer serializer, Object source, String name) {
        try {
            char[] chars = name.toCharArray();
            chars[0] -= 32;
            String m_name = "get" + new String(chars);
            Method method = clazz.getMethod(m_name);
            JsonSubString include = method.getAnnotation(JsonSubString.class);
            if (include != null){
                String setName = "set" + new String(chars);
                Method setMethod = clazz.getMethod(setName, String.class);
                String content = (String) method.invoke(source, null);
                content = FrontUtils.substring(content, include.length());
                setMethod.invoke(source, content);
            }
        } catch (Exception e){
            e.printStackTrace();
        } finally{
            return true;
        }
    }
}
