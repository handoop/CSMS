package org.victoria.common.front.helper;

import com.alibaba.fastjson.serializer.JSONSerializer;
import org.victoria.common.annotation.JsonIncludeField;

import java.lang.reflect.Method;
import java.util.Arrays;

/**
 * Created by Administrator on 2015-3-18.
 */
public class SimpleJsonFilter extends AbstractJsonFilter {

    private String namespace;

    public SimpleJsonFilter(String namespace){
        this.namespace = namespace;
    }

    @Override
    public boolean doSomething(JSONSerializer serializer, Object source, String name) {
        try {
            char[] chars = name.toCharArray();
            chars[0] -= 32;
            String m_name = "get" + new String(chars);
            Method method = clazz.getMethod(m_name);
            JsonIncludeField include = method.getAnnotation(JsonIncludeField.class);
            if (include != null){
                if (Arrays.asList(include.namespace()).contains(namespace))
                    return true;
            }
            return false;
        } catch (Exception e) {
            return false;
        }
    }

}
