package org.victoria.common.front.tag;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.tagext.SimpleTagSupport;
import java.io.IOException;
import java.io.StringWriter;
import java.io.Writer;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by Administrator on 2015-1-30.
 */
public class DateFormatter extends SimpleTagSupport{

    private Date value;
    private String format = "yyyy-MM-dd HH:mm:ss";

    @Override
    public void doTag() throws JspException, IOException {
        if (value!=null){
            SimpleDateFormat format = new SimpleDateFormat(getFormat());
            String date_str = format.format(value);
            Writer writer = new StringWriter();
            writer.write(date_str);
            getJspContext().getOut().write(date_str);
        }
    }

    public String getFormat() {
        return format;
    }

    public void setFormat(String format) {
        this.format = format;
    }

    public void setValue(Date value) {
        this.value = value;
    }
}
