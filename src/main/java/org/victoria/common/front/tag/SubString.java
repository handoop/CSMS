package org.victoria.common.front.tag;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.tagext.SimpleTagSupport;
import java.io.IOException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by Administrator on 2015-2-1.
 */
public class SubString extends SimpleTagSupport{

    private String value;
    private Integer length;

    @Override
    public void doTag() throws JspException, IOException {
        Pattern pattern = Pattern.compile("<[^>]+>|&nbsp");
        Matcher matcher = pattern.matcher(value);
        value = matcher.replaceAll("");
        getJspContext().getOut().write(value.length()<length ? value : value.substring(0, length)+"···");
    }

    public Integer getLength() {
        return length;
    }

    public void setLength(Integer length) {
        this.length = length;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }
}
