package org.victoria.common.front.tag;

import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

import javax.servlet.jsp.PageContext;
import javax.servlet.jsp.tagext.SimpleTagSupport;

/**
 * Created by Administrator on 2015-1-26.
 */
public class SpringTagAdapter extends SimpleTagSupport {


    public <T> T getBean(Class<T> clazz){
        PageContext pageContext = (PageContext) getJspContext();
        WebApplicationContext context = WebApplicationContextUtils.getRequiredWebApplicationContext(pageContext.getServletContext());
        return context.getBean(clazz);
    }

}
