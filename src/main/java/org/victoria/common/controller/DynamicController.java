package org.victoria.common.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.victoria.common.utils.FrontUtils;
import org.victoria.common.utils.StringUtils;

import javax.servlet.http.HttpServletRequest;

/**
 * Created by thanatos on 15-6-2.
 */
@Controller
public class DynamicController extends BaseController{

    private static final Logger logger = LoggerFactory.getLogger(DynamicController.class);

    /**
     * 首页入口
     * @param request
     * @return
     */
    @RequestMapping(value = {"/", "index.htm"}, method = RequestMethod.GET)
    public String index(HttpServletRequest request){
        String uri = FrontUtils.getURI(request);
        //除了“/”之外的请求地址都被认为是非法的
        if (!StringUtils.isEmpty(uri) && !uri.equals("/"))
            return FrontUtils.PAGE_NOT_FOUND;
        return "admin/index";
    }

}
