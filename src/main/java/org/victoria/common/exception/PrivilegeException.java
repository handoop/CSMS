package org.victoria.common.exception;

/**
 * Created by Administrator on 2015-1-21.
 */
public class PrivilegeException extends RuntimeException {

    public PrivilegeException() {
        super();
    }

    public PrivilegeException(String message) {
        super(message);
    }

    public PrivilegeException(String message, Throwable cause) {
        super(message, cause);
    }

    public PrivilegeException(Throwable cause) {
        super(cause);
    }
}
