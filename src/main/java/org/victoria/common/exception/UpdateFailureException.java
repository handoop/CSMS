package org.victoria.common.exception;

/**
 * Created by Administrator on 2015-1-21.
 */
public class UpdateFailureException extends RuntimeException {

    public UpdateFailureException() {
        super();
    }

    public UpdateFailureException(String message) {
        super(message);
    }

    public UpdateFailureException(String message, Throwable cause) {
        super(message, cause);
    }

    public UpdateFailureException(Throwable cause) {
        super(cause);
    }
}
