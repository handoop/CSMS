package org.victoria.common.exception;

/**
 * Created by Administrator on 2015-1-21.
 */
public class ReduplicativeException extends RuntimeException {

    public ReduplicativeException() {
        super();
    }

    public ReduplicativeException(String message) {
        super(message);
    }

    public ReduplicativeException(String message, Throwable cause) {
        super(message, cause);
    }

    public ReduplicativeException(Throwable cause) {
        super(cause);
    }
}
