package org.victoria.common.dao;

import org.victoria.common.domain.Page;
import org.victoria.common.utils.QueryHelper;

import java.io.Serializable;
import java.util.List;

/**
 * Created by Administrator on 14-8-17.
 */
public interface DaoSupport<T> {

    /**
     * 保存
     * @param t
     */
    void save(T t) ;

    /**
     * 更新
     * @param t
     */
    void update(T t) ;

    /**
     * 通过辅助类更新
     * @param helper
     */
    @Deprecated
    void update(QueryHelper helper);

    /**
     * 更新指定注解上的匹配名称空间的字段，注意！它只适用于使用注解映射实体的情况
     * @param object
     */
    void update(T object, String namespace) throws Exception;

    /**
     * 通过id查找
     * @param id
     */
    T getById(Serializable id) ;
    /**
     *
     * @param helper 辅助类
     * @param cacheable 是否缓存
     * @return
     */
    List<T> find(QueryHelper helper, boolean cacheable);
    /**
     *
     * @param helper
     * @param cacheable
     * @return
     */
    T findUnique(QueryHelper helper, boolean cacheable);

    /**
     * 得到所有
     * @param cacheable
     * @return
     */
    List<T> findAll(boolean cacheable);
    /**
     * 物理上的删除
     * @param id
     */
    @Deprecated
    void absoluteDelete(Serializable id);

    /**
     * 分页接口
     * @param pageNum
     * @param helper
     * @param pageSize
     * @return
     */
    Page<T> getPage(int pageNum, QueryHelper helper, int pageSize);

    /**
     * 分页接口
     * @param pageNum
     * @param helper
     * @param pageSize
     * @return
     */
    Page<T> getPage(int pageNum, QueryHelper helper, int pageSize, boolean cacheable);

    /**
     * 数值自减1
     * @param id
     * @param clause
     */
    void autoDecrease(Serializable id, String clause);

    /**
     * 自动增值或降值
     * @param id 对象id
     * @param clause 指定操作的列
     * @param up 如果为true，则自增1，如果为false，则自减1
     */
    void autoUpOrDown(Serializable id, String clause, boolean up);

    /**
     * 得到记录数量
     * @param helper
     * @param cacheable 是否缓存
     * @return
     */
    int getCount(QueryHelper helper, boolean cacheable);

    /**
     *
     * @param id
     */
    void delete(Long id);

    /**
     * 获取单页数据
     * @param pageNum
     * @param helper
     * @param pageSize
     * @return
     */
    List<T> getSinglePage(int pageNum, QueryHelper helper, int pageSize, boolean cacheable);
}
