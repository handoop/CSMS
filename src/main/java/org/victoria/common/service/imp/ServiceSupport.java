package org.victoria.common.service.imp;

import org.victoria.common.dao.imp.DaoSupportImp;
import org.victoria.common.service.BaseService;

import javax.transaction.Transactional;
import java.util.List;

/**
 * Created by handoop on 2015/4/2.
 */
@Transactional
public abstract class ServiceSupport<T> extends DaoSupportImp<T> implements BaseService<T> {

    @Override
    public T saveEntity(T obj) {
        dao.save(obj);
        return obj;
    }

    @Override
    public T updateEntity(T obj) {
        return null;
    }

    @Override
    public void deleteEntity(T obj) {
        dao.getSession().delete(obj);
    }

    @Override
    public void deleteById(Long id) {
        dao.delete(id);
    }

    @Override
    public List<T> getAll() {
        return dao.findAll(true);
    }

    @Override
    public T getById(Long id) {
        return id == null ? null : dao.getById(id);
    }

    @Override
    public T saveOrUpdate(T entity) {
        if (entity==null)
            return null;
        dao.getSession().saveOrUpdate(entity);
        return entity;
    }

    @Override
    public void batchDelete(Long[] ids) {
        if (ids !=null && ids.length>0){
            for (Long id : ids){
                T entity = dao.getById(id);
                dao.getSession().delete(entity);
            }
        }
    }

}
