package org.victoria.common.service;

import java.util.List;

/**
 * Created by handoop on 2015/4/2.
 */
public interface BaseService<T> {

    /**
     *
     * @param obj
     * @return
     */
    T saveEntity(T obj);

    /**
     *
     * @param obj
     */
    void deleteEntity(T obj);

    /**
     *
     * @param obj
     * @return
     */
    T updateEntity(T obj);

    /**
     *
     * @param id
     */
    void deleteById(Long id);

    /**
     *
     * @return
     */
    List<T> getAll();

    /**
     *
     * @param id
     * @return
     */
    T getById(Long id);

    /**
     *
     * @param ids
     */
    void batchDelete(Long[] ids);

    /**
     *
     * @param entity
     * @return
     */
    T saveOrUpdate(T entity);

}
