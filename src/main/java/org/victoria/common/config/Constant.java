package org.victoria.common.config;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by Administrator on 2015-1-17.
 */
public class Constant {

    private static Map<String, Integer> typeMap;

    public static Map<String, Integer> getDataTypeMap(){
        if (typeMap==null){
            typeMap = new HashMap<String, Integer>();
            typeMap.put("java.lang.Long", 0);
            typeMap.put("java.lang.Integer", 1);
            typeMap.put("java.lang.Float", 2);
            typeMap.put("java.lang.Double", 3);
            typeMap.put("java.lang.String", 4);
            typeMap.put("java.util.Date", 5);
            typeMap.put("java.lang.Boolean", 6);
            typeMap.put("java.lang.Short", 7);
        }
        return typeMap;
    }

}
