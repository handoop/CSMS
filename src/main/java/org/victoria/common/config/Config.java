package org.victoria.common.config;

/**
 * Created by Administrator on 2015-1-4.
 */
public final class Config {

    /**
     * 分页导航栏中允许显示多少页
     */
    public static final int PAGE_NAVIGATE = 5;
    /**
     * 默认一页的数据量
     */
    public static final int DEFAULT_PAGESIZE = 10;

    /**
     * 图片上传支持的图片
     */
    public static final String IMAGE_SUFFIX = "jpg,jpeg,png,bmp";
    /**
     * 回复一页显示的数量
     */
    public static final int REPLY_PAGE_SIZE = 8;
    /**
     * 其他职位
     */
    public static final int OTHERS_PAGE_SIZE = 5;
    /**
     * 一次获得的最大留言数量
     */
    public static final int MESSAGE_MAX_SIZE = 8;
    /**
     * 显示热门职位的数量
     */
    public static final int HOT_POSITION_PAGE_NUM = 8;
    /**
     * 正在审核
     */
    public static final int CHECKING = 1;
    /**
     * 还没有校验
     */
    public static final int UNCHECKED = 0;
    /**
     * 校验失败
     */
    public static final int FAILURE_CHECK = 2;
    /**
     * 校验成功
     */
    public static final int CHECK_SUCCESS = 3;
    /**
     * 用户动态记录类型——回复
     */
    public static final int DYNAMIC_TYPE_REPLY = 0;
    /**
     * 用户动态记录类型——留言
     */
    public static final int DYNAMIC_TYPE_MESSAGE = 1;
    /**
     * 会员个人中心问答单页显示记录
     */
    public static final int PERSONAL_QUESTION_PAGE_SIZE = 8;
    /**
     * 会员中心留言单页显示记录
     */
    public static final int PERSONAL_MESSAGE_PAGE_SIZE = 8;
    /**
     * 动态信息显示多少条
     */
    public static int DYNAMIC_PAGE_SIZE = 8;
    /**
     *
     */
    public static int DEFAULT_PAGE_SIZE_FOR_ADMIN = 40;

    /**
     * 文章默认标题图
     */
    public static final String ARTICLE_DEFAULT_TITLE_IMAGE = "/upload/images/_default/default_picture.png";

}
