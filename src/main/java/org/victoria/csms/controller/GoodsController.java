package org.victoria.csms.controller;

import com.alibaba.fastjson.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.victoria.common.controller.BaseController;
import org.victoria.common.domain.Page;
import org.victoria.common.utils.ResponseUtil;
import org.victoria.csms.domain.GoodsColor;
import org.victoria.csms.domain.GoodsSize;
import org.victoria.csms.domain.Goods;
import org.victoria.csms.service.GoodsColorService;
//import org.victoria.csms.service.GoodsService;
import org.victoria.csms.service.GoodsSizeService;
import org.victoria.csms.service.GoodsService;
import org.victoria.csms.util.NumberUtil;

import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import java.util.List;

/**
 * Created by xiaomin on 2015/6/4.
 */
@Controller
public class GoodsController extends BaseController {

    private static final Logger logger = LoggerFactory.getLogger(UserController.class);
//    @Autowired
//    private GoodsService goodsService;
    @Autowired
    private GoodsService goodsService;
    @Autowired
    private GoodsColorService GoodsColorServiceImp;
    @Autowired
    private GoodsSizeService GoodsSizeServiceImp;

    @RequestMapping("/admin/goods/listType.htm")
    public String list(Model model, Integer pageNum, String goods_number,String goods_name,String goods_color,String goods_size){
        pageNum = pageNum == null ? 1 : pageNum;
        Page<Goods> page = goodsService.getList(pageNum,goods_number,goods_name,goods_color,goods_size);
        model.addAttribute(page);
        model.addAttribute("goods_number", goods_number);
        model.addAttribute("goods_name", goods_name);
        model.addAttribute("goods_color", goods_color);
        model.addAttribute("goods_size", goods_size);

        List<GoodsColor> colors = GoodsColorServiceImp.getAll();
        System.out.println(colors.toString());
        List<GoodsSize> sizes = GoodsSizeServiceImp.getAll();
        System.out.println(sizes.toString());
        model.addAttribute("colors", colors);
        model.addAttribute("sizes",sizes);

        return "admin/goods/listType";
    }

    @RequestMapping(value = "/admin/goods/addType.htm", method = RequestMethod.GET)
    public String addType(Goods goodstype,Model model){
        List<GoodsColor> colors = GoodsColorServiceImp.getAll();
        System.out.println(colors.toString());
        List<GoodsSize> sizes = GoodsSizeServiceImp.getAll();
        System.out.println(sizes.toString());
        model.addAttribute("colors", colors);
        model.addAttribute("sizes",sizes);
        model.addAttribute("goods_number", NumberUtil.getOrderNumber("GD"));
        return "admin/goods/addType";
    }

    @RequestMapping(value = "/admin/goods/addType.htm", method = RequestMethod.POST)
    public String addType(@Valid Goods goods, BindingResult result){
        System.out.println(goods);
        if (result.hasErrors()){
            logger.error("storage form has error");
            return "admin/goods/addType";
        }
        Goods gs = goodsService.getByNumber(goods.getGoods_number());
        if(gs == null){
            goodsService.saveEntity(goods);
        } else {
            gs.setFabric(goods.getFabric());
            gs.setFactoryPrice(goods.getFactoryPrice());
            gs.setGoods_color(goods.getGoods_color());
            gs.setGoods_name(goods.getGoods_name());
            gs.setGoods_size(goods.getGoods_size());
            gs.setMaterial(goods.getMaterial());
            gs.setRetailPrice(goods.getRetailPrice());
            goodsService.updateObj(gs);
        }

        logger.error("redirect goods list");
        return "redirect:/admin/goods/listType.htm";
    }

    @RequestMapping(value="/admin/goods/delete.htm",method = RequestMethod.POST)
    public void delete(Long id, HttpServletResponse response){
        JSONObject object = ResponseUtil.getJsonObject();
        if (id==null){
            object.put("code", 1);
            object.put("message", "The parameter had mistake");
            ResponseUtil.renderJson(response, object.toJSONString());
        }
        goodsService.deleteById(id);
        object.put("code",0);
        object.put("message", "delete successful");
        ResponseUtil.renderJson(response, object.toJSONString());
    }

    @RequestMapping("/admin/goods/detail.htm")
    public String detail(Goods goods,Long id,Model model){
        List<GoodsColor> colors = GoodsColorServiceImp.getAll();
        System.out.println(colors.toString());
        List<GoodsSize> sizes = GoodsSizeServiceImp.getAll();
        System.out.println(sizes.toString());
        model.addAttribute("colors", colors);
        model.addAttribute("sizes",sizes);

        Goods g = goodsService.getById(id);
        model.addAttribute("goods_number", g.getGoods_number());
        model.addAttribute("goods_color",g.getGoods_color());
        model.addAttribute("goods_size",g.getGoods_size());
        model.addAttribute("goods_name",g.getGoods_name());
        model.addAttribute("fabric",g.getFabric());
        model.addAttribute("material",g.getMaterial());
        model.addAttribute("factoryPrice",g.getFactoryPrice());
        model.addAttribute("retailPrice",g.getRetailPrice());
        return "admin/goods/addType";
    }


}
