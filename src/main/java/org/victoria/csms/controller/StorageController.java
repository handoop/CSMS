package org.victoria.csms.controller;

import com.alibaba.fastjson.JSONObject;
import org.apache.poi.util.SystemOutLogger;
import org.hibernate.annotations.SourceType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.victoria.common.controller.BaseController;
import org.victoria.common.domain.Page;
import org.victoria.common.utils.ResponseUtil;
import org.victoria.csms.domain.Storage;
import org.victoria.csms.service.StorageService;
import org.victoria.csms.util.NumberUtil;

import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by xiaomin on 2015/6/4.
 */
@Controller
public class StorageController  extends BaseController {

    private static final Logger logger = LoggerFactory.getLogger(UserController.class);
    @Autowired
    private StorageService manager;

    @RequestMapping("/admin/storage/list.htm")
    public String list(Model model, Integer pageNum, String sto_number,String sto_name){
        pageNum = pageNum == null ? 1 : pageNum;
        Page<Storage> page = manager.getList(pageNum, sto_number,sto_name);
        model.addAttribute(page);
        model.addAttribute("sto_number", sto_number);
        model.addAttribute("sto_name", sto_name);
        return "admin/storage/list";
    }

    @RequestMapping(value = "/admin/storage/add.htm", method = RequestMethod.GET)
    public String add(Storage storage,Model model){
        model.addAttribute("sto_number", NumberUtil.getOrderNumber("STG"));
        return "admin/storage/add";
    }

    @RequestMapping(value = "/admin/storage/add.htm", method = RequestMethod.POST)
    public String add(@Valid Storage storage, BindingResult result){
        if (result.hasErrors()){
            logger.error("storage form has error");
            return "admin/storage/add";
        }

        Storage st = manager.getByNumber(storage.getSto_number());
        if(st == null){
            manager.saveEntity(storage);
        } else {

            st.setConnect_man(storage.getConnect_man());
            st.setConnect_phone(storage.getConnect_phone());
            st.setSto_name(storage.getSto_name());
            manager.updateObj(st);
        }


        logger.error("redirect storage list");
        return "redirect:/admin/storage/list.htm";
    }

    @RequestMapping("/admin/storage/delete.htm")
    public void delete(Long id, HttpServletResponse response){
        JSONObject object = ResponseUtil.getJsonObject();
        if (id==null){
            object.put("code", 1);
            object.put("message", "The parameter had mistake");
            ResponseUtil.renderJson(response, object.toJSONString());
        }
        manager.deleteById(id);
        object.put("code",0);
        object.put("message", "delete successful");
        ResponseUtil.renderJson(response, object.toJSONString());
    }

    @RequestMapping("/admin/storage/detail")
    public String detail(Storage storage,Long id,Model model){
        System.out.println(id);
        Storage s = manager.getById(id);
        model.addAttribute("sto_number",s.getSto_number());
        model.addAttribute("sto_name",s.getSto_name());
        model.addAttribute("connect_man",s.getConnect_man());
        model.addAttribute("connect_phone",s.getConnect_phone());
//        model.addAttribute("sto_capacity",s.getSto_capacity());
        return "admin/storage/add";
    }
}
