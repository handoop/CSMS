package org.victoria.csms.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.victoria.common.controller.BaseController;
import org.victoria.common.utils.FrontUtils;

import javax.servlet.http.HttpServletRequest;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by xiaomin on 2015/6/3.
 */
@Controller
public class PathController extends BaseController {

    @RequestMapping("/admin/**/*.htm")
    public String dynamic(HttpServletRequest request){
        String path = FrontUtils.getURI(request);
        path = path.replace(".htm", "");
        Pattern pattern = Pattern.compile("/");
        Matcher matcher = pattern.matcher(path);
        path = matcher.replaceFirst("");
        return path;
    }
}
