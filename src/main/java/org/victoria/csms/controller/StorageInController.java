package org.victoria.csms.controller;

import com.alibaba.fastjson.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.ServletRequestDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.RequestMapping;
import org.victoria.common.controller.BaseController;
import org.victoria.common.domain.Page;
import org.victoria.common.utils.ResponseUtil;
import org.victoria.csms.domain.*;
import org.victoria.csms.service.*;
import org.victoria.csms.util.NumberUtil;

import javax.servlet.http.HttpServletResponse;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

/**
 * Created by hmaccelerate on 2015/6/9.
 */
@Controller
@RequestMapping("/admin/stock")
public class StorageInController extends BaseController {
    private static final Logger logger = LoggerFactory.getLogger(StorageInController.class);
    @Autowired
    private StorageInService storageInService;
    @Autowired
    private StorageService storageService;
    @Autowired
    private GoodsService goodsService;
    @Autowired
    private StorageInDetailService storageInDetailService;
    @Autowired
    private GoodsStorageService goodsStorageService;

    @InitBinder
    public void initBinder(ServletRequestDataBinder binder) {
        binder.registerCustomEditor(Date.class,
                new CustomDateEditor(new SimpleDateFormat("yyyy-MM-dd"), true));
    }


    @RequestMapping("/showStorageIn.htm")
    public String showStorageIn(Model model, Integer pageNum,
                                String storageID, String storageIn_number,
                                Date storageIn_dateStartTime, Date storageIn_dateEndTime) {
        System.out.println(storageIn_dateStartTime);
        System.out.println(storageIn_dateEndTime);

        List<Storage> storageList = storageService.getAll();
        model.addAttribute("storageList", storageList);
        pageNum = pageNum == null ? 1 : pageNum;
        Page<StorageIn> storageInPage = storageInService.getList(pageNum, storageID,
                storageIn_number, storageIn_dateStartTime, storageIn_dateEndTime);

        model.addAttribute("storageInPage", storageInPage);
        model.addAttribute("storageID", storageID);
        model.addAttribute("storageIn_number", storageIn_number);

        if (storageIn_dateStartTime != null && storageIn_dateEndTime != null) {
            model.addAttribute("storageIn_dateStartTime",
                    new SimpleDateFormat("yyyy-MM-dd").format(storageIn_dateStartTime));

            model.addAttribute("storageIn_dateEndTime",
                    new SimpleDateFormat("yyyy-MM-dd").format(storageIn_dateEndTime));
        }
        return "admin/stock/storageIn";
    }

    @RequestMapping("/showOrderInAdd.htm")
    public String showOrderInAdd(Model model) {
        List<Storage> storageList = storageService.getAll();
        model.addAttribute("storageList", storageList);
        String storageIn_number = NumberUtil.getOrderNumber("RC");
        model.addAttribute("storageIn_number", storageIn_number);
        return "admin/stock/orderInAdd";
    }

    @RequestMapping("/addStorageInOrder.htm")
    public String addStorageOrder(StorageIn storageIn, Long storageID, Date storageIn_dateTime) {
        System.out.println(storageID);
        System.out.println(storageIn_dateTime);
        Storage storage = new Storage();
        storage.setId(storageID);
        storageIn.setStorage(storage);
        System.out.println(storageIn.toString());
        storageInService.saveEntity(storageIn);
        return "redirect:showStorageIn.htm";
    }

    @RequestMapping("/delStorageInOrder.htm")
    public void delStorageOrder(Long storageInID, HttpServletResponse response) {
        JSONObject object = ResponseUtil.getJsonObject();
        if (storageInID == null) {
            object.put("code", 1);
            object.put("message", "Mistake Happened");
            ResponseUtil.renderJson(response, object.toJSONString());
        }
        if (storageInDetailService.getList(1, storageInID).getRecordList().size() == 0) {
            storageInService.deleteById(storageInID);
            object.put("code", 0);
            object.put("message", "delete successful");
            ResponseUtil.renderJson(response, object.toJSONString());
        } else {
            object.put("code", 2);
            object.put("message", "入库单下有详情，无法删除");
            ResponseUtil.renderJson(response, object.toJSONString());
        }

    }


    @RequestMapping("/showStorageInDetail.htm")
    public String showStorageInDetail(Model model, long id, Integer pageNum) {
        System.out.println(id);
        StorageIn storageInDetail = storageInService.getById(id);
        model.addAttribute("storageInDetail", storageInDetail);
        pageNum = pageNum == null ? 1 : pageNum;
        Page<StorageInDetail> page = storageInDetailService.getList(pageNum, id);
        model.addAttribute("page", page);
        return "admin/stock/storageInDetail";
    }

    @RequestMapping("/showStorageInDetailAdd.htm")
    public String showStorageInDetailAdd(Model model, long storageInID, long storageID) {
        model.addAttribute("storageInID", storageInID);
        List<Goods> goodsList = goodsService.getAll();
        model.addAttribute("goodsList", goodsList);
        model.addAttribute("storageID", storageID);
        return "admin/stock/orderInDetailAdd";
    }

    @RequestMapping("/storageInDetailAdd.htm")
    public String storageInDetailAdd(Model model, StorageInDetail detail,
                                     Long storageInID, Long goodsID, long storageID) {
        System.out.println(detail.getStorageInNum());
        StorageIn storageIn = new StorageIn();
        storageIn.setStorageInID(storageInID);
        Goods goods = new Goods();
        goods.setGoodsID(goodsID);
        detail.setGoods(goods);
        detail.setStorageIn(storageIn);
        storageInDetailService.saveEntity(detail);

        Goods_Storage goodsStorage = goodsStorageService.getByGoodsStorageID(goodsID, storageID);
        if (goodsStorage == null) {
            Goods_Storage goods_storage = new Goods_Storage();
            goods_storage.setGoods(goods);
            Storage storage = new Storage();
            storage.setId(storageID);
            goods_storage.setStorage(storage);
            goods_storage.setStorageSum(detail.getStorageInNum() + "");
            goodsStorageService.saveEntity(goods_storage);
        } else {
            String oldStorageSum = goodsStorage.getStorageSum();
            int newStorageSum = Integer.parseInt(oldStorageSum) + detail.getStorageInNum();
            goodsStorage.setStorageSum(newStorageSum + "");
            goodsStorageService.saveOrUpdate(goodsStorage);
        }
        model.addAttribute("id", storageInID);
        return "redirect:showStorageInDetail.htm";
    }


    @RequestMapping("/getGoodsMessage.htm")
    public void getGoodsMessage(Long goodsID,HttpServletResponse response){
        JSONObject object = ResponseUtil.getJsonObject();
        if (goodsID==null){
            object.put("code", 1);
            object.put("message", "Mistake Happened");
            ResponseUtil.renderJson(response, object.toJSONString());
        }
        Goods goods=goodsService.getById(goodsID);
        object.put("code", 0);
        object.put("goods",goods);
        ResponseUtil.renderJson(response, object.toJSONString());
    }

    @RequestMapping("/storageInDetailDel.htm")
    public void storageInDetailDel(Long storageInDetailID, Long storageID,HttpServletResponse response) {
        JSONObject object = ResponseUtil.getJsonObject();
        if (storageInDetailID == null&&storageID==null) {
            object.put("code", 1);
            object.put("message", "Mistake Happened");
            ResponseUtil.renderJson(response, object.toJSONString());
        }
        StorageInDetail detail = storageInDetailService.getById(storageInDetailID);
        Long goodsID = detail.getGoods().getGoodsID();
        Goods_Storage goodsStorage = goodsStorageService.getByGoodsStorageID(goodsID, storageID);
        String oldStorageSum = goodsStorage.getStorageSum();
        int newStorageSum=Integer.parseInt(oldStorageSum)-detail.getStorageInNum();
        goodsStorage.setStorageSum(newStorageSum+"");
        goodsStorageService.saveOrUpdate(goodsStorage);
        storageInDetailService.deleteById(storageInDetailID);
        object.put("code", 0);
        object.put("message", "删除成功");
        ResponseUtil.renderJson(response, object.toJSONString());
    }

}
