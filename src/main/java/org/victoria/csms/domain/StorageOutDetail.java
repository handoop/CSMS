package org.victoria.csms.domain;

import java.util.Date;
import java.util.Set;

/**
 * @author guishen
 */
public class StorageOutDetail {
    private Long storageOutDetailID;
    private int goodsNumber;
    private StorageOut storageOut;
    private Goods Goods;

    public org.victoria.csms.domain.Goods getGoods() {
        return Goods;
    }

    public void setGoods(org.victoria.csms.domain.Goods goods) {
        Goods = goods;
    }

    public int getGoodsNumber() {
        return goodsNumber;
    }

    public void setGoodsNumber(int goodsNumber) {
        this.goodsNumber = goodsNumber;
    }

    public Long getStorageOutDetailID() {
        return storageOutDetailID;
    }

    public void setStorageOutDetailID(Long storageOutDetailID) {
        this.storageOutDetailID = storageOutDetailID;
    }

    public StorageOut getStorageOut() {
        return storageOut;
    }

    public void setStorageOut(StorageOut storageOut) {
        this.storageOut = storageOut;
    }
}
