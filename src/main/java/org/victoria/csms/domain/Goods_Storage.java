package org.victoria.csms.domain;

/**
 * Created by xiaomin on 2015/6/11.
 */
public class Goods_Storage {
    private Long id;
    private String storageSum;

    // many to one
    private Storage storage;
    private Goods goods;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getStorageSum() {
        return storageSum;
    }

    public void setStorageSum(String storageSum) {
        this.storageSum = storageSum;
    }

    public Storage getStorage() {
        return storage;
    }

    public void setStorage(Storage storage) {
        this.storage = storage;
    }

    public Goods getGoods() {
        return goods;
    }

    public void setGoods(Goods goods) {
        this.goods = goods;
    }
}
