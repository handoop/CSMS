package org.victoria.csms.domain;

import java.util.Set;

/**
 * Created by xiaomin on 2015/6/4.
 */
public class GoodsColor {

    private Long id;
    private String Color;

//    // one to many
//    private Set<Goods> goodses;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getColor() {
        return Color;
    }

    public void setColor(String color) {
        Color = color;
    }

//    public Set<Goods> getGoodses() {
//        return goodses;
//    }
//
//    public void setGoodses(Set<Goods> goodses) {
//        this.goodses = goodses;
//    }

    @Override
    public String toString() {
        return "GoodsColor{" +
                "id=" + id +
                ", Color='" + Color + '\'' +
//                ", goodses=" + goodses +
                '}';
    }
}
