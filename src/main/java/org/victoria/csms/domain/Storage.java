package org.victoria.csms.domain;

import java.util.Set;

/**
 * Created by xiaomin on 2015/6/4.
 */
public class Storage {

    private Long id;
    private String sto_number;
    private String sto_name;
//    private int sto_capacity;
    private  String connect_man;
    private String connect_phone;

//    one to many
//    private Set<Goods> goodsTypes;
    private Set<Goods_Storage> goods_storages;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getSto_number() {
        return sto_number;
    }

    public void setSto_number(String sto_number) {
        this.sto_number = sto_number;
    }

    public String getSto_name() {
        return sto_name;
    }

    public void setSto_name(String sto_name) {
        this.sto_name = sto_name;
    }

//    public int getSto_capacity() {
//        return sto_capacity;
//    }
//
//    public void setSto_capacity(int sto_capacity) {
//        this.sto_capacity = sto_capacity;
//    }

    public String getConnect_man() {
        return connect_man;
    }

    public void setConnect_man(String connect_man) {
        this.connect_man = connect_man;
    }

    public String getConnect_phone() {
        return connect_phone;
    }

    public void setConnect_phone(String connect_phone) {
        this.connect_phone = connect_phone;
    }

//    public Set<Goods> getGoodsTypes() {
//        return goodsTypes;
//    }
//
//    public void setGoodsTypes(Set<Goods> goodsTypes) {
//        this.goodsTypes = goodsTypes;
//    }


    public Set<Goods_Storage> getGoods_storages() {
        return goods_storages;
    }

    public void setGoods_storages(Set<Goods_Storage> goods_storages) {
        this.goods_storages = goods_storages;
    }
}
