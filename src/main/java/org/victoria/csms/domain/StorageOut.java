package org.victoria.csms.domain;

import java.util.Date;
import java.util.Set;

/**
 * @author guishen
 */
public class StorageOut {
    private Long storageOutID;
    private String storageOutNumber;
    private String receiverName;
    private String receiverPhone;
    private String receiverAddress;
    private Date storageOutDateTime;
    private String storageOutNote;

    private Storage storage;
    private Set<StorageOutDetail> storageOutDetailSet;

    public Set<StorageOutDetail> getStorageOutDetailSet() {
        return storageOutDetailSet;
    }

    public void setStorageOutDetailSet(Set<StorageOutDetail> storageOutDetailSet) {
        this.storageOutDetailSet = storageOutDetailSet;
    }

    public Storage getStorage() {
        return storage;
    }

    public void setStorage(Storage storage) {
        this.storage = storage;
    }

    public Long getStorageOutID() {
        return storageOutID;
    }

    public void setStorageOutID(Long storageOutID) {
        this.storageOutID = storageOutID;
    }

    public String getStorageOutNumber() {
        return storageOutNumber;
    }

    public void setStorageOutNumber(String storageOutNumber) {
        this.storageOutNumber = storageOutNumber;
    }

    public String getReceiverName() {
        return receiverName;
    }

    public void setReceiverName(String receiverName) {
        this.receiverName = receiverName;
    }

    public String getReceiverPhone() {
        return receiverPhone;
    }

    public void setReceiverPhone(String receiverPhone) {
        this.receiverPhone = receiverPhone;
    }

    public String getReceiverAddress() {
        return receiverAddress;
    }

    public void setReceiverAddress(String receiverAddress) {
        this.receiverAddress = receiverAddress;
    }

    public Date getStorageOutDateTime() {
        return storageOutDateTime;
    }

    public void setStorageOutDateTime(Date storageOutDateTime) {
        this.storageOutDateTime = storageOutDateTime;
    }

    public String getStorageOutNote() {
        return storageOutNote;
    }

    public void setStorageOutNote(String storageOutNote) {
        this.storageOutNote = storageOutNote;
    }
}
