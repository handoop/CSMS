package org.victoria.csms.domain;

/**
 * Created by acer on 2015/6/9.
 */
public class StorageInDetail {
    private Long storageInDetailID;
    private StorageIn storageIn;
    private Goods goods;
    private int storageInNum;

//

    public Long getStorageInDetailID() {
        return storageInDetailID;
    }

    public void setStorageInDetailID(Long storageInDetailID) {
        this.storageInDetailID = storageInDetailID;
    }

    public StorageIn getStorageIn() {
        return storageIn;
    }

    public void setStorageIn(StorageIn storageIn) {
        this.storageIn = storageIn;
    }

    public Goods getGoods() {
        return goods;
    }

    public void setGoods(Goods goods) {
        this.goods = goods;
    }

    public int getStorageInNum() {
        return storageInNum;
    }

    public void setStorageInNum(int storageInNum) {
        this.storageInNum = storageInNum;
    }
}
