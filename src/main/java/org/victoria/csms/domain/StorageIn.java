package org.victoria.csms.domain;

import java.util.Date;

/**
 * Created by acer on 2015/6/9.
 */
public class StorageIn {
    private Long storageInID;
    private String storageIn_number;
    private String receiver_man;
    private Date storageIn_dateTime;
    private String source_company;
    private String storageIn_note;
    private Storage storage;

//    public int getStorageInID() {
//        return storageInID;
//    }
//
//    public void setStorageInID(int storageInID) {
//        this.storageInID = storageInID;
//    }


    public Long getStorageInID() {
        return storageInID;
    }

    public void setStorageInID(Long storageInID) {
        this.storageInID = storageInID;
    }

    public String getStorageIn_number() {
        return storageIn_number;
    }

    public void setStorageIn_number(String storageIn_number) {
        this.storageIn_number = storageIn_number;
    }

    public String getReceiver_man() {
        return receiver_man;
    }

    public void setReceiver_man(String receiver_man) {
        this.receiver_man = receiver_man;
    }

    public Date getStorageIn_dateTime() {
        return storageIn_dateTime;
    }

    public void setStorageIn_dateTime(Date storageIn_dateTime) {
        this.storageIn_dateTime = storageIn_dateTime;
    }

    public String getSource_company() {
        return source_company;
    }

    public void setSource_company(String source_company) {
        this.source_company = source_company;
    }

    public String getStorageIn_note() {
        return storageIn_note;
    }

    public void setStorageIn_note(String storageIn_note) {
        this.storageIn_note = storageIn_note;
    }

    public Storage getStorage() {
        return storage;
    }

    public void setStorage(Storage storage) {
        this.storage = storage;
    }

    @Override
    public String toString() {
        return "StorageIn{" +
                "storageInID=" + storageInID +
                ", storageIn_number='" + storageIn_number + '\'' +
                ", receiver_man='" + receiver_man + '\'' +
                ", storageIn_dateTime=" + storageIn_dateTime +
                ", source_company='" + source_company + '\'' +
                ", storageIn_note='" + storageIn_note + '\'' +
                ", storage=" + storage +
                '}';
    }
}
