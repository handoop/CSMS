package org.victoria.csms.domain;

import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.NotBlank;

import java.util.Date;
import java.util.Set;

/**
 * Created by thanatos on 15-6-2.
 */
public class User {

    //field
    private Long id;
    private String name;
    private String username;
    private String password;
    private String summary;
    private Date createTime;

    //one to many
    private Set<Role> roles;

    //get and set
    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Set<Role> getRoles() {
        return roles;
    }

    @NotBlank
    @Length(min = 1, max = 20, message = "The minimum is 1 and the maximum is 20")
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSummary() {
        return summary;
    }

    public void setSummary(String summary) {
        this.summary = summary;
    }

    public void setRoles(Set<Role> roles) {
        this.roles = roles;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @NotBlank
    @Length(min = 1, max = 20, message = "The minimum is 1 and the maximum is 20")
    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }


    @NotBlank
    @Length(min = 1, max = 150, message = "The minimum is 1 and the maximum is 20")
    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
