package org.victoria.csms.domain;

import java.util.Set;

/**
 * Created by handoop on 2015/3/31.
 */
public class Role {

    //field
    private Long id;
    private String name;

    //many to many
    private Set<User> users;
    private Set<Privilege> privileges;

    //get and set
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Set<User> getUsers() {
        return users;
    }

    public void setUsers(Set<User> users) {
        this.users = users;
    }

    public Set<Privilege> getPrivileges() {
        return privileges;
    }

    public void setPrivileges(Set<Privilege> privileges) {
        this.privileges = privileges;
    }
}
