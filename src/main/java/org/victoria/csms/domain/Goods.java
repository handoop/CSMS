package org.victoria.csms.domain;

import java.util.Set;

/**
 * Created by xiaomin on 2015/6/4.
 */
public class Goods {

    private Long goodsID;
    private String goods_number;
    private String goods_name;
    private String goods_color;
    private String goods_size;
    private String factoryPrice;
    private String retailPrice;
    private String fabric;
    private String material;

    // one to one
//    private Storage storage;

    // one to many
//    private Set<Goods> goodses;

    // one to many
//    private Set<Storage> storages;
    private Set<Goods_Storage> goods_storages;

//    public Storage getStorage() {
//        return storage;
//    }

//    public void setStorage(Storage storage) {
//        this.storage = storage;
//    }


    public Long getGoodsID() {
        return goodsID;
    }

    public void setGoodsID(Long goodsID) {
        this.goodsID = goodsID;
    }

    public String getGoods_number() {
        return goods_number;
    }

    public void setGoods_number(String goods_number) {
        this.goods_number = goods_number;
    }

    public String getGoods_name() {
        return goods_name;
    }

    public void setGoods_name(String goods_name) {
        this.goods_name = goods_name;
    }

    public String getGoods_color() {
        return goods_color;
    }

    public void setGoods_color(String goods_color) {
        this.goods_color = goods_color;
    }

    public String getGoods_size() {
        return goods_size;
    }

    public void setGoods_size(String goods_size) {
        this.goods_size = goods_size;
    }

    public String getFactoryPrice() {
        return factoryPrice;
    }

    public void setFactoryPrice(String factoryPrice) {
        this.factoryPrice = factoryPrice;
    }

    public String getRetailPrice() {
        return retailPrice;
    }

    public void setRetailPrice(String retailPrice) {
        this.retailPrice = retailPrice;
    }

    public String getFabric() {
        return fabric;
    }

    public void setFabric(String fabric) {
        this.fabric = fabric;
    }

    public String getMaterial() {
        return material;
    }

    public void setMaterial(String material) {
        this.material = material;
    }
//
//    public Set<Goods> getGoodses() {
//        return goodses;
//    }
//
//    public void setGoodses(Set<Goods> goodses) {
//        this.goodses = goodses;
//    }


//    public Set<Storage> getStorages() {
//        return storages;
//    }
//
//    public void setStorages(Set<Storage> storages) {
//        this.storages = storages;
//    }


    public Set<Goods_Storage> getGoods_storages() {
        return goods_storages;
    }

    public void setGoods_storages(Set<Goods_Storage> goods_storages) {
        this.goods_storages = goods_storages;
    }

    @Override
    public String toString() {
        return "GoodsType{" +
                "goodsID=" + goodsID +
                ", goods_number='" + goods_number + '\'' +
                ", goods_name='" + goods_name + '\'' +
                ", goods_color='" + goods_color + '\'' +
                ", goods_size='" + goods_size + '\'' +
                ", factoryPrice='" + factoryPrice + '\'' +
                ", retailPrice='" + retailPrice + '\'' +
                ", fabric='" + fabric + '\'' +
                ", material='" + material + '\'' +
//                ", storages=" + storages +
                '}';
    }
}
