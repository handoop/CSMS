package org.victoria.csms.domain;

import java.util.Set;

/**
 * Created by handoop on 2015/3/31.
 */
public class Privilege {

    //field
    private Long id;
    private String name;

    //many to many
    private Set<Role> roles;

    //get and set
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Set<Role> getRoles() {
        return roles;
    }

    public void setRoles(Set<Role> roles) {
        this.roles = roles;
    }
}
