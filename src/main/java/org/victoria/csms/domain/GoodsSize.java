package org.victoria.csms.domain;

import java.util.Set;

/**
 * Created by xiaomin on 2015/6/4.
 */
public class GoodsSize {

    private Long id;
    private String size;

//    private Set<Goods> goodses;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getSize() {
        return size;
    }

    public void setSize(String size) {
        this.size = size;
    }

//    public Set<Goods> getGoodses() {
//        return goodses;
//    }

//    public void setGoodses(Set<Goods> goodses) {
//        this.goodses = goodses;
//    }

    @Override
    public String toString() {
        return "GoodsSize{" +
                "id=" + id +
                ", size='" + size + '\'' +
//                ", goodses=" + goodses +
                '}';
    }
}
