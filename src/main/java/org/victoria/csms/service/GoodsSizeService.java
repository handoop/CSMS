package org.victoria.csms.service;

import org.victoria.common.service.BaseService;
import org.victoria.csms.domain.GoodsSize;

/**
 * Created by xiaomin on 2015/6/6.
 */
public interface GoodsSizeService extends BaseService<GoodsSize> {
}
