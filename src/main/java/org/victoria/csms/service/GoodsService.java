package org.victoria.csms.service;

import org.victoria.common.domain.Page;
import org.victoria.common.service.BaseService;
import org.victoria.csms.domain.Goods;

/**
 * Created by xiaomin on 2015/6/6.
 */
public interface GoodsService extends BaseService<Goods> {
    Page<Goods> getList(Integer pageNum,String goods_number,String goods_name,String goods_color,String goods_size );
    Goods getByNumber(String number);
    void updateObj(Goods goods);
}
