package org.victoria.csms.service;

import org.victoria.common.domain.Page;
import org.victoria.common.service.BaseService;
import org.victoria.csms.domain.GoodsColor;

import java.util.List;

/**
 * Created by xiaomin on 2015/6/6.
 */
public interface GoodsColorService extends BaseService<GoodsColor>{


}
