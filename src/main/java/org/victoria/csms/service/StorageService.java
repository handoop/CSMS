package org.victoria.csms.service;

import org.victoria.common.domain.Page;
import org.victoria.common.service.BaseService;
import org.victoria.csms.domain.Storage;

import java.util.List;

/**
 * Created by xiaomin on 2015/6/4.
 */
public interface StorageService extends BaseService<Storage> {

    Page<Storage> getList(Integer pageNum, String sto_number,String sto_name);
    Storage getByNumber(String number);
    void updateObj(Storage storage);

}
