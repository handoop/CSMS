package org.victoria.csms.service;

import org.victoria.common.domain.Page;
import org.victoria.common.service.BaseService;
import org.victoria.csms.domain.User;

/**
 * Created by thanatos on 15-6-2.
 */
public interface UserService extends BaseService<User>{

    User getUserByAccount(String username);

    Page<User> getList(Integer pageNum, String name, String username);

    User getByAccAndPass(String username, String password);

    void updatePassword(Long id, String password);
}
