package org.victoria.csms.service;

import org.victoria.common.domain.Page;
import org.victoria.common.service.BaseService;
import org.victoria.csms.domain.Goods_Storage;
import org.victoria.csms.domain.StorageOut;

/**
 * Created by thanatos on 15-6-2.
 */
public interface GoodsStorageService extends BaseService<Goods_Storage>{

    public Page<Goods_Storage> getList(Integer pageNum, String name);

    public Goods_Storage getByGoodsStorageID(Long goodsID, Long storageID);

}
