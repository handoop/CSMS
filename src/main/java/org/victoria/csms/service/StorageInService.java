package org.victoria.csms.service;

import org.victoria.common.domain.Page;
import org.victoria.common.service.BaseService;
import org.victoria.csms.domain.Storage;
import org.victoria.csms.domain.StorageIn;

import java.util.Date;
import java.util.List;

/**
 * Created by acer on 2015/6/9.
 */
public interface StorageInService extends BaseService<StorageIn>{
    Page<StorageIn> getList(Integer pageNum,String storageID,String storageIn_number,Date storageIn_dateStartTime,Date storageIn_dateEndTime);
}
