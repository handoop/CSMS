package org.victoria.csms.service.imp;

import org.springframework.stereotype.Service;
import org.victoria.common.domain.Page;
import org.victoria.common.service.imp.ServiceSupport;
import org.victoria.common.utils.QueryHelper;
import org.victoria.common.utils.StringUtils;
import org.victoria.csms.domain.Goods;
import org.victoria.csms.service.GoodsService;

import javax.transaction.Transactional;

/**
 * Created by xiaomin on 2015/6/6.
 */
@Service
@Transactional
public class GoodsServiceImp extends ServiceSupport<Goods> implements GoodsService {
    @Override
    public Page<Goods> getList(Integer pageNum,String goods_number,String goods_name,String goods_color,String goods_size) {
        QueryHelper helper = dao.getSelectHelper();
        helper.where(!StringUtils.isEmpty(goods_number), "goods_number", goods_number)
                .where(!StringUtils.isEmpty(goods_name), "goods_name", goods_name)
                .where(!StringUtils.isEmpty(goods_color), "goods_color", goods_color)
                .where(!StringUtils.isEmpty(goods_size), "goods_size", goods_size)
                .orderBy("id", true);
        return dao.getPage(pageNum, helper, 5);
    }


    public Goods getByNumber(String number){
        QueryHelper helper = dao.getSelectHelper();
        helper.where(!StringUtils.isEmpty(number), "goods_number", number);
        return dao.findUnique(helper,false);
    }

    @Override
    public void updateObj(Goods goods) {
        dao.getSession().update(goods);
    }
}
