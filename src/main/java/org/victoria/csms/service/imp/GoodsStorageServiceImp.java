package org.victoria.csms.service.imp;


import org.springframework.stereotype.Service;
import org.victoria.common.domain.Page;
import org.victoria.common.service.imp.ServiceSupport;
import org.victoria.common.utils.QueryHelper;
import org.victoria.csms.domain.Goods_Storage;
import org.victoria.csms.service.GoodsStorageService;

import javax.transaction.Transactional;
import java.util.List;

/**
 * Created by Greyson on 2015/6/17.
 */
@Service
@Transactional
public class GoodsStorageServiceImp extends ServiceSupport<Goods_Storage> implements GoodsStorageService{
    @Override
    public Page<Goods_Storage> getList(Integer pageNum, String name) {
        return null;
    }

    @Override
    public Goods_Storage getByGoodsStorageID(Long goodsID, Long storageID) {
        System.out.println(getClass().getSimpleName()+"'s getByGoodsStorageID: "+goodsID+" -- "+storageID);
        QueryHelper helper = dao.getSelectHelper();
        helper.where("goodsID", goodsID).where("storageID", storageID);
        List list = dao.find(helper, true);
        if(list == null || list.size() == 0)
            return null;
        else
            return (Goods_Storage)list.get(0);
    }
}
