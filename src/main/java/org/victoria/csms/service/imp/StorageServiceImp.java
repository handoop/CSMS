package org.victoria.csms.service.imp;

import org.springframework.stereotype.Service;
import org.victoria.common.domain.Page;
import org.victoria.common.service.imp.ServiceSupport;
import org.victoria.common.utils.QueryHelper;
import org.victoria.common.utils.StringUtils;
import org.victoria.csms.domain.Storage;
import org.victoria.csms.service.StorageService;

import javax.transaction.Transactional;
import java.util.List;

/**
 * Created by xiaomin on 2015/6/4.
 */
@Service
@Transactional
public class StorageServiceImp extends ServiceSupport<Storage> implements StorageService {

    @Override
    public Page<Storage> getList(Integer pageNum, String sto_number,String sto_name) {

        QueryHelper helper = dao.getSelectHelper();
        helper.where(!StringUtils.isEmpty(sto_number), "sto_number", sto_number)
                .where(!StringUtils.isEmpty(sto_name), "sto_name", sto_name)
                .orderBy("id", true);
        return dao.getPage(pageNum, helper, 5);
    }

    @Override
    public Storage getByNumber(String number) {
        QueryHelper helper = dao.getSelectHelper();
        helper.where(!StringUtils.isEmpty(number), "sto_number", number);
        return dao.findUnique(helper,false);
    }

    @Override
    public void updateObj(Storage storage) {
        dao.getSession().update(storage);
    }
}
