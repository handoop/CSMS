package org.victoria.csms.service.imp;

import org.springframework.stereotype.Service;
import org.victoria.common.domain.Page;
import org.victoria.common.service.imp.ServiceSupport;
import org.victoria.common.utils.QueryHelper;
import org.victoria.common.utils.StringUtils;
import org.victoria.csms.domain.StorageOut;
import org.victoria.csms.service.StorageOutService;

import javax.transaction.Transactional;

/**
 * Created by thanatos on 15-6-2.
 */
@Service
@Transactional
public class StorageOutServiceImp extends ServiceSupport<StorageOut> implements StorageOutService{

    @Override
    public Page<StorageOut> getList(Integer pageNum, String storageID, String storageOutNumber, String startTime, String endTime) {
        System.out.println("storageOutService's getList -- storageID:" + storageID + " --page: " + pageNum+" --start:"+startTime+" --end:"+endTime+" --storageOutNumber:"+storageOutNumber);
        QueryHelper helper = dao.getSelectHelper();
        helper.where(!StringUtils.isEmpty(storageID), "storageID", storageID)
                .where(!StringUtils.isEmpty(storageOutNumber), "storageOutNumber", storageOutNumber)
                .orderBy("id", true);
        if(!StringUtils.isEmpty(startTime)){
            if(!StringUtils.isEmpty(endTime)){
                helper.between("storageOutDateTime", startTime, endTime);
            }else{
                helper.where("storageOutDateTime > '"+startTime+"'");
            }
        }else{
            if(!StringUtils.isEmpty(endTime)){
                helper.where("storageOutDateTime < '"+endTime+"'");
            }
        }
        System.out.println("storageOutService's helper: "+helper.get__where_clause__());;
//        helper.select("storageOutID", "storageOutNumber", "receiverName", "receiverPhone", "receiverAddress", "storageOutDateTime", "storageOutNote", "storageID");
        return dao.getPage(pageNum, helper, 3);
    }

}
