package org.victoria.csms.service.imp;

import org.springframework.stereotype.Service;
import org.victoria.common.service.imp.ServiceSupport;
import org.victoria.common.utils.QueryHelper;
import org.victoria.csms.domain.GoodsColor;
import org.victoria.csms.service.GoodsColorService;

import javax.transaction.Transactional;
import java.util.List;

/**
 * Created by xiaomin on 2015/6/6.
 */
@Service
@Transactional
public class GoodsColorServiceImp  extends ServiceSupport<GoodsColor> implements GoodsColorService {

}
