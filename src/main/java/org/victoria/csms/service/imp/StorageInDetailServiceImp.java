package org.victoria.csms.service.imp;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.victoria.common.domain.Page;
import org.victoria.common.service.imp.ServiceSupport;
import org.victoria.common.utils.QueryHelper;
import org.victoria.csms.domain.StorageInDetail;
import org.victoria.csms.service.StorageInDetailService;

/**
 * Created by hmaccelerate on 2015/6/21.
 */
@Service
@Transactional
public class StorageInDetailServiceImp extends ServiceSupport<StorageInDetail> implements StorageInDetailService {
    @Override
    public Page<StorageInDetail> getList(Integer pageNum,long storageInID) {
        QueryHelper queryHelper=dao.getSelectHelper();
        queryHelper.where("storageInID",storageInID).orderBy("storageInDetailID",true);
        return dao.getPage(pageNum,queryHelper,5);
    }
}
