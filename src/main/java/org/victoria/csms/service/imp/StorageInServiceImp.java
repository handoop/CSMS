package org.victoria.csms.service.imp;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.victoria.common.domain.Page;
import org.victoria.common.service.imp.ServiceSupport;
import org.victoria.common.utils.QueryHelper;
import org.victoria.common.utils.StringUtils;
import org.victoria.csms.domain.StorageIn;
import org.victoria.csms.service.StorageInService;

import java.util.Date;


/**
 * Created by hmaccelerate on 2015/6/9.
 */
@Service
@Transactional
public class StorageInServiceImp extends ServiceSupport<StorageIn> implements StorageInService {
    @Override
    public Page<StorageIn> getList(Integer pageNum, String storageID,
                                   String storageIn_number, Date storageIn_dateStartTime, Date storageIn_dateEndTime) {
        QueryHelper helper = dao.getSelectHelper();
        System.out.println(StringUtils.isEmpty(storageID + ""));
        if (!StringUtils.isEmpty(storageIn_number)) {
            helper.where("storageIn_number", storageIn_number);
        }
        helper.where(!StringUtils.isEmpty(storageID), "storageID", storageID).orderBy("storageInID",true);
        if (storageIn_dateStartTime != null && storageIn_dateEndTime != null) {
            helper.between("storageIn_dateTime", storageIn_dateStartTime, storageIn_dateEndTime);
        }
        return dao.getPage(pageNum, helper, 5);
    }
}
