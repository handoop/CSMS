package org.victoria.csms.service.imp;

import org.springframework.stereotype.Service;
import org.victoria.common.domain.Page;
import org.victoria.common.utils.QueryHelper;
import org.victoria.csms.domain.StorageOutDetail;
import org.victoria.csms.service.StorageOutDetailService;
import org.victoria.common.service.imp.ServiceSupport;

import javax.transaction.Transactional;

/**
 * Created by Greyson on 2015/6/6.
 */
@Service
@Transactional
public class StorageOutDetailServiceImp extends ServiceSupport<StorageOutDetail> implements StorageOutDetailService{

    @Override
    public Page<StorageOutDetail> getList(Integer pageNum, String storageOutID) {
        System.out.println("storageOutDetailService's getList -- storageOutID:" + storageOutID);
        QueryHelper helper = dao.getSelectHelper();
        helper.where("storageOutID", storageOutID);
        return dao.getPage(pageNum, helper, 5);
    }
}
