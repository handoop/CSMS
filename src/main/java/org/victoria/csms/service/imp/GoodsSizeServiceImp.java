package org.victoria.csms.service.imp;

import org.springframework.stereotype.Service;
import org.victoria.common.service.imp.ServiceSupport;
import org.victoria.csms.domain.GoodsSize;
import org.victoria.csms.service.GoodsSizeService;

import javax.transaction.Transactional;
import java.util.List;

/**
 * Created by xiaomin on 2015/6/6.
 */
@Service
@Transactional
public class GoodsSizeServiceImp  extends ServiceSupport<GoodsSize> implements GoodsSizeService {

}
