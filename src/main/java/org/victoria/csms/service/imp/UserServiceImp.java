package org.victoria.csms.service.imp;

import org.springframework.stereotype.Service;
import org.victoria.common.domain.Page;
import org.victoria.common.service.imp.ServiceSupport;
import org.victoria.common.utils.QueryHelper;
import org.victoria.common.utils.StringUtils;
import org.victoria.csms.domain.User;
import org.victoria.csms.service.UserService;

import javax.transaction.Transactional;
import java.util.Date;

/**
 * Created by thanatos on 15-6-2.
 */
@Service
@Transactional
public class UserServiceImp extends ServiceSupport<User> implements UserService{

    @Override
    public User getUserByAccount(String username) {
        QueryHelper helper = dao.getSelectHelper()
                .where("username", username);
        return dao.findUnique(helper, false);
    }

    @Override
    public Page<User> getList(Integer pageNum, String name, String username) {
        QueryHelper helper = dao.getSelectHelper();
        helper.like(!StringUtils.isEmpty(name), "name", "%" + name + "%")
                .like(!StringUtils.isEmpty(username), "username", "%" + username + "%")
                .orderBy("createTime", true);
        return dao.getPage(pageNum, helper, 5);
    }

    @Override
    public User getByAccAndPass(String account, String password) {
        QueryHelper helper = dao.getSelectHelper()
                .where("username", account)
                .where("password", password);
        return dao.findUnique(helper, false);
    }

    @Override
    public void updatePassword(Long id, String password) {
        User user = (User) dao.getSession().get(User.class, id);
        System.out.println("update user is "+user);
        if (user!=null)
            user.setPassword(password);
        System.out.println("done");
    }

}
