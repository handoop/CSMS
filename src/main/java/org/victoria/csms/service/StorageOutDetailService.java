package org.victoria.csms.service;

import org.victoria.common.domain.Page;
import org.victoria.common.service.BaseService;
import org.victoria.csms.domain.StorageOut;
import org.victoria.csms.domain.StorageOutDetail;

/**
 * Created by thanatos on 15-6-2.
 */
public interface StorageOutDetailService extends BaseService<StorageOutDetail>{
    /**
     *
     * @param pageNum the page will be indicated
     * @param storageOutID
     * @return
     */
    public Page<StorageOutDetail> getList(Integer pageNum, String storageOutID);

}
