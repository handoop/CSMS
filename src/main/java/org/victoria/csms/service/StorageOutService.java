package org.victoria.csms.service;

import org.victoria.common.domain.Page;
import org.victoria.common.service.BaseService;
import org.victoria.csms.domain.StorageOut;
import org.victoria.csms.domain.User;

import java.util.List;

/**
 * Created by thanatos on 15-6-2.
 */
public interface StorageOutService extends BaseService<StorageOut>{

    public Page<StorageOut> getList(Integer pageNum, String storageID, String storageOutID, String startTime, String endTime);

}
