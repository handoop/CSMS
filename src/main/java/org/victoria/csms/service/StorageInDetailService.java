package org.victoria.csms.service;

import org.victoria.common.domain.Page;
import org.victoria.common.service.BaseService;
import org.victoria.csms.domain.StorageInDetail;

/**
 * Created by hmaccelerate on 2015/6/21.
 */

public interface StorageInDetailService extends BaseService<StorageInDetail>  {
    Page<StorageInDetail> getList(Integer pageNum,long storageInID);
}
