package org.victoria.csms.util;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by Greyson on 2015/6/11.
 */
public class NumberUtil {

    /**
     * create a number accordding the date with the given prefix
     * @param prefix
     * @return
     */
    public static String getOrderNumber(String prefix){
        DateFormat format = new SimpleDateFormat("yyyyMMddHHmmss");
        return prefix + format.format(new Date());
    }
}
